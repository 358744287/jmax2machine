package com.xdtech.base.util;

/**
 * @author Jason 20130313
 * 用于人事业务常用的常量
 */
public class ProConstUtil
{
	public static final int STATE_USABLE = 1;// 可用
	public static final int STATE_LOAN = 2;// 借出 
	public static final int STATE_LOSE = 3;// 丢失
	public static final int STATE_REPAIR = 4;// 维修
	public static final int STATE_RETURN_NEW = 5;// 返厂翻新
	
	public static final int RESPONSE_NO = 1;// 未审批
	public static final int RESPONSE_YES = 2;// 审批通过
	
	public static final int NOT_RETURN = 0;// 未归还
	public static final int IS_RETURN = 1;// 已归还
}
