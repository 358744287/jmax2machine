package com.xdtech.base.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.xdtech.core.model.BaseModel;

/**
 * 
 * @author max.zheng
 * @create 2017-03-07 15:35:59
 * @since 1.0
 * @see 
 */
@Entity
@Table(name="BASE_MACHINE_TYPE")
public class BaseMachineType extends BaseModel implements Serializable{
	private static final long serialVersionUID = 1L;
	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	@Column(name="TYPE")
	private String type;
	
	@Column(name="NAME")
	private String name;
	
	@Column(name="BRAND")
	private String brand;
	
	public BaseMachineType() {
		super();
	}

	//一对多
	@OneToMany(fetch=FetchType.LAZY,mappedBy="baseMachineType")
	List<BaseMachineStock> baseMachineStocks = new ArrayList<BaseMachineStock>();

	public void setId(Long id) {
		this.id = id;
	}
	public Long getId() {
		return id;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getType() {
		return type;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getName() {
		return name;
	}
	public void setBrand(String brand) {
		this.brand = brand;
	}
	public String getBrand() {
		return brand;
	}

	public List<BaseMachineStock> getBaseMachineStocks()
	{
		return baseMachineStocks;
	}

	public void setBaseMachineStocks(List<BaseMachineStock> baseMachineStocks)
	{
		this.baseMachineStocks = baseMachineStocks;
	}
}
