package com.xdtech.base.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Date;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import com.xdtech.core.model.BaseModel;

/**
 * 
 * @author max.zheng
 * @create 2017-03-05 13:19:30
 * @since 1.0
 * @see 
 */
@Entity
@Table(name="BASE_MACHINE_REMARK_CHANGE")
public class BaseMachineRemarkChange extends BaseModel implements Serializable{
	private static final long serialVersionUID = 1L;
	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	@Column(name="DEV_SN")
	private String devSn;
	
	@Column(name="ORDER_NO")
	private String orderNo;
	
	@Column(name="REMARK")
	private String remark;
	
	@Column(name="REMARK_CHANGE")
	private String remarkChange;
	
	@Column(name="CHANGE_TIME")
	private Date changeTime;
	
	@Column(name="DEV_NAME")
	private String devName;
	
	public BaseMachineRemarkChange() {
		super();
	}


	public void setId(Long id) {
		this.id = id;
	}
	public Long getId() {
		return id;
	}
	public void setDevSn(String devSn) {
		this.devSn = devSn;
	}
	public String getDevSn() {
		return devSn;
	}
	public void setOrderNo(String orderNo) {
		this.orderNo = orderNo;
	}
	public String getOrderNo() {
		return orderNo;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemarkChange(String remarkChange) {
		this.remarkChange = remarkChange;
	}
	public String getRemarkChange() {
		return remarkChange;
	}
	public void setChangeTime(Date changeTime) {
		this.changeTime = changeTime;
	}
	public Date getChangeTime() {
		return changeTime;
	}
	public void setDevName(String devName) {
		this.devName = devName;
	}
	public String getDevName() {
		return devName;
	}
}
