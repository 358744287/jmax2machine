package com.xdtech.base.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.xdtech.core.model.BaseModel;

/**
 * 
 * @author max.zheng
 * @create 2017-03-07 15:36:05
 * @since 1.0
 * @see 
 */
@Entity
@Table(name="BASE_MACHINE_STOCK")
public class BaseMachineStock extends BaseModel implements Serializable{
	private static final long serialVersionUID = 1L;
	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	@Column(name="DEV_SN")
	private String devSn;
	
	@Column(name="ORDER_NO")
	private String orderNo;
	
	@Column(name="REMARK")
	private String remark;
	
	@Column(name="STATE")
	private Integer state;
	
	@Column(name="INTO_DATE")
	private Date intoDate;
	
	@Column(name="COUNTS")
	private Integer counts;
	
	public BaseMachineStock() {
		super();
	}

	//多对一
	@ManyToOne
    @JoinColumn(name = "DEV_TYPE_ID")
    BaseMachineType baseMachineType;

	public void setId(Long id) {
		this.id = id;
	}
	public Long getId() {
		return id;
	}
	public void setDevSn(String devSn) {
		this.devSn = devSn;
	}
	public String getDevSn() {
		return devSn;
	}
	public void setOrderNo(String orderNo) {
		this.orderNo = orderNo;
	}
	public String getOrderNo() {
		return orderNo;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public String getRemark() {
		return remark;
	}
	public void setState(Integer state) {
		this.state = state;
	}
	public Integer getState() {
		return state;
	}
	public void setIntoDate(Date intoDate) {
		this.intoDate = intoDate;
	}
	public Date getIntoDate() {
		return intoDate;
	}
	public void setCounts(Integer counts) {
		this.counts = counts;
	}
	public Integer getCounts() {
		return counts;
	}

	public BaseMachineType getBaseMachineType()
	{
		return baseMachineType;
	}
	public void setBaseMachineType(BaseMachineType baseMachineType)
	{
		this.baseMachineType = baseMachineType;
	}
}
