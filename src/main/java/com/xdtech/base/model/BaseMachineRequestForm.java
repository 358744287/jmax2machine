package com.xdtech.base.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Date;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import com.xdtech.core.model.BaseModel;

/**
 * 
 * @author max.zheng
 * @create 2017-03-05 12:36:02
 * @since 1.0
 * @see 
 */
@Entity
@Table(name="BASE_MACHINE_REQUEST_FORM")
public class BaseMachineRequestForm extends BaseModel implements Serializable{
	private static final long serialVersionUID = 1L;
	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	@Column(name="DEPT_NAME")
	private String deptName;
	
	@Column(name="EMP_PIN")
	private String empPin;
	
	@Column(name="NAME")
	private String name;
	
	@Column(name="DEV_IDS")
	private String devIds;
	
	@Column(name="REQUEST_CONTENT",length=500)
	private String requestContent;
	
	@Column(name="REQUEST_DATE")
	private String requestDate;
	
	@Column(name="REQUEST_STATE")
	private Integer requestState;
	
	@Column(name="RESPONSE_DATE")
	private String responseDate;
	
	@Column(name="REMARK")
	private String remark;
	
	public BaseMachineRequestForm() {
		super();
	}


	public void setId(Long id) {
		this.id = id;
	}
	public Long getId() {
		return id;
	}
	public void setDeptName(String deptName) {
		this.deptName = deptName;
	}
	public String getDeptName() {
		return deptName;
	}
	public void setEmpPin(String empPin) {
		this.empPin = empPin;
	}
	public String getEmpPin() {
		return empPin;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getName() {
		return name;
	}
	public void setDevIds(String devIds) {
		this.devIds = devIds;
	}
	public String getDevIds() {
		return devIds;
	}
	public void setRequestContent(String requestContent) {
		this.requestContent = requestContent;
	}
	public String getRequestContent() {
		return requestContent;
	}
	public void setRequestDate(String requestDate) {
		this.requestDate = requestDate;
	}
	public String getRequestDate() {
		return requestDate;
	}
	public void setRequestState(Integer requestState) {
		this.requestState = requestState;
	}
	public Integer getRequestState() {
		return requestState;
	}
	public void setResponseDate(String responseDate) {
		this.responseDate = responseDate;
	}
	public String getResponseDate() {
		return responseDate;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public String getRemark() {
		return remark;
	}

}
