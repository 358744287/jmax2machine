package com.xdtech.base.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Date;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import com.xdtech.core.model.BaseModel;

/**
 * 
 * @author max.zheng
 * @create 2017-03-05 12:56:55
 * @since 1.0
 * @see
 */
@Entity
@Table(name = "BASE_MACHINE_BORROW")
public class BaseMachineBorrow extends BaseModel implements Serializable
{
	private static final long serialVersionUID = 1L;
	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@Column(name = "DEV_ID")
	private Long devId;

	@Column(name = "DEV_NAME")
	private String devName;

	@Column(name = "DEV_TYPE")
	private String devType;

	@Column(name = "DEV_SN")
	private String devSn;

	@Column(name = "ORDER_NO")
	private String orderNo;

	@Column(name = "BORROW_DEPT")
	private String borrowDept;

	@Column(name = "BORROW_EMP")
	private String borrowEmp;

	@Column(name = "EMP_PIN")
	private String empPin;

	@Column(name = "BORROW_DATE")
	private String borrowDate;

	@Column(name = "RETURN_DATE")
	private String returnDate;

	@Column(name = "REMARK")
	private String remark;

	@Column(name = "INTO_DATE")
	private String intoDate;

	@Column(name = "IS_RETURN")
	private Integer isReturn;

	public BaseMachineBorrow()
	{
		super();
	}

	public void setId(Long id)
	{
		this.id = id;
	}

	public Long getId()
	{
		return id;
	}

	public void setDevId(Long devId)
	{
		this.devId = devId;
	}

	public Long getDevId()
	{
		return devId;
	}

	public void setDevName(String devName)
	{
		this.devName = devName;
	}

	public String getDevName()
	{
		return devName;
	}

	public void setDevType(String devType)
	{
		this.devType = devType;
	}

	public String getDevType()
	{
		return devType;
	}

	public void setDevSn(String devSn)
	{
		this.devSn = devSn;
	}

	public String getDevSn()
	{
		return devSn;
	}

	public void setOrderNo(String orderNo)
	{
		this.orderNo = orderNo;
	}

	public String getOrderNo()
	{
		return orderNo;
	}

	public void setBorrowDept(String borrowDept)
	{
		this.borrowDept = borrowDept;
	}

	public String getBorrowDept()
	{
		return borrowDept;
	}

	public void setBorrowEmp(String borrowEmp)
	{
		this.borrowEmp = borrowEmp;
	}

	public String getBorrowEmp()
	{
		return borrowEmp;
	}

	public void setEmpPin(String empPin)
	{
		this.empPin = empPin;
	}

	public String getEmpPin()
	{
		return empPin;
	}

	public void setRemark(String remark)
	{
		this.remark = remark;
	}

	public String getRemark()
	{
		return remark;
	}

	public void setIsReturn(Integer isReturn)
	{
		this.isReturn = isReturn;
	}

	public Integer getIsReturn()
	{
		return isReturn;
	}

	public String getBorrowDate()
	{
		return borrowDate;
	}

	public void setBorrowDate(String borrowDate)
	{
		this.borrowDate = borrowDate;
	}

	public String getReturnDate()
	{
		return returnDate;
	}

	public void setReturnDate(String returnDate)
	{
		this.returnDate = returnDate;
	}

	public String getIntoDate()
	{
		return intoDate;
	}

	public void setIntoDate(String intoDate)
	{
		this.intoDate = intoDate;
	}
}
