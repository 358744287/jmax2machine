package com.xdtech.base.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.xdtech.core.model.BaseModel;
import com.xdtech.sys.model.UserGroup;
/**
 * 
 * @author max.zheng
 * @create 2017-03-05 11:52:17
 * @since 1.0
 * @see
 */
@Entity
@Table(name = "BASE_PERSON")
public class BasePerson extends BaseModel implements Serializable
{
	private static final long serialVersionUID = 1L;
	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@Column(name = "PIN")
	private String pin;

	@Column(name = "NAME")
	private String name;

	@OneToOne
	@JoinColumn(name = "USER_GROUP_ID")
	private UserGroup userGroup;
	//创建人员与用户的关系
	@Column(name = "SYS_USER_ID")
	private Long userId;

	public BasePerson()
	{
		super();
	}

	public void setId(Long id)
	{
		this.id = id;
	}

	public Long getId()
	{
		return id;
	}

	public void setPin(String pin)
	{
		this.pin = pin;
	}

	public String getPin()
	{
		return pin;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public String getName()
	{
		return name;
	}

	public Long getUserId()
	{
		return userId;
	}

	public void setUserId(Long userId)
	{
		this.userId = userId;
	}

	public UserGroup getUserGroup()
	{
		return userGroup;
	}

	public void setUserGroup(UserGroup userGroup)
	{
		this.userGroup = userGroup;
	}

}
