package com.xdtech.base.controller;


import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.xdtech.sys.aspect.SystemControllerLog;
import com.xdtech.sys.util.SessionContextUtil;
import com.xdtech.sys.vo.UserItem;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.xdtech.base.service.BaseMachineRequestFormService;
import com.xdtech.base.vo.BaseMachineRequestFormItem;
import com.xdtech.core.init.InitCacheData;
import com.xdtech.core.orm.utils.BeanUtils;
import com.xdtech.web.controller.BaseController;
import com.xdtech.web.model.ComboBox;
import com.xdtech.web.model.Pagination;
import com.xdtech.web.model.ResultMessage;

/**
 * 
 * @author max.zheng
 * @create 2017-03-05 12:36:02
 * @since 1.0
 * @see
 */
@Controller
@Scope("prototype")
public class BaseMachineRequestFormController extends BaseController
{
	@Autowired
	private BaseMachineRequestFormService baseMachineRequestFormService;
	@RequestMapping(value="/baseMachineRequestForm.do",params = "baseMachineRequestForm")
	public ModelAndView skipBaseMachineRequestForm() 
	{
		return new ModelAndView("base/baseMachineRequestForm/baseMachineRequestForm_ftl");
	}
	
	@RequestMapping(value="/baseMachineRequestForm.do",params="loadList")
	@ResponseBody
	public Map<String, Object> loadList(BaseMachineRequestFormItem item,Pagination pg) 
	{
		Map<String, Object> results =  baseMachineRequestFormService.loadPageDataByConditions(pg,item,"findBaseMachineRequestFormByCondition");
		return results;
	}
	
	@RequestMapping(value="/baseMachineRequestForm.do",params = "editBaseMachineRequestForm")
	@SystemControllerLog(description = "编辑")
	public ModelAndView editBaseMachineRequestForm(HttpServletRequest request,Long baseMachineRequestFormId)
	{
		if (baseMachineRequestFormId!=null) 
		{
			request.setAttribute("baseMachineRequestFormItem", baseMachineRequestFormService.loadBaseMachineRequestFormItem(baseMachineRequestFormId));
		}
		UserItem user = SessionContextUtil.getCurrentUser();
		request.setAttribute("currentUser", user);
		return new ModelAndView("base/baseMachineRequestForm/editBaseMachineRequestForm_ftl");
	}
	
	@RequestMapping(value="/baseMachineRequestForm.do",params = "saveBaseMachineRequestForm")
	@ResponseBody
	@SystemControllerLog(description = "保存")
	public ResultMessage saveBaseMachineRequestForm(BaseMachineRequestFormItem item)
	{
		ResultMessage r = new ResultMessage();
		if (baseMachineRequestFormService.saveOrUpdateBaseMachineRequestForm(item))
		{
			r.setSuccess(true);
		}
		else
		{
			r.setSuccess(false);
		}
		return r;
	}
	
	@RequestMapping(value="/baseMachineRequestForm.do",params = "deleteBaseMachineRequestFormItems")
	@ResponseBody
	@SystemControllerLog(description = "删除")
	public ResultMessage deleteBaseMachineRequestFormItems(String ids)
	{
		ResultMessage r = new ResultMessage();
		if (StringUtils.isNotEmpty(ids))
		{
			String[] tempIds = ids.split(",");
			List<Long> baseMachineRequestFormIds = new ArrayList<Long>();
			for (String id : tempIds) 
			{
				baseMachineRequestFormIds.add(Long.valueOf(id));
			}
			baseMachineRequestFormService.deleteBaseMachineRequestFormInfo(baseMachineRequestFormIds);
			r.setSuccess(true);
		}
		else
		{
			r.setSuccess(false);
		}
		return r;
	}
	
	@RequestMapping(value="/baseMachineRequestForm.do",params = "passBaseMachineRequestFormItems")
	@ResponseBody
	@SystemControllerLog(description = "申请通过")
	public ResultMessage passBaseMachineRequestFormItems(String ids)
	{
		ResultMessage r = new ResultMessage();
		if (StringUtils.isNotEmpty(ids))
		{
			baseMachineRequestFormService.passMachineRequestForm(ids + "-1");
			r.setSuccess(true);
		}
		else
		{
			r.setSuccess(false);
		}
		return r;
	}
	
	@RequestMapping(params = "loadComboBox")
	@ResponseBody
	public Object loadComboBox(String key,String value,String text,String isSearch) {
		List items = InitCacheData.dictionary.get(key);
		List<ComboBox> comboBoxs = BeanUtils.copyListProperties(ComboBox.class, items);
		if ("true".equals(isSearch)) {
			comboBoxs.add(0,new ComboBox("", "------", false));
		}
		for (ComboBox comboBox : comboBoxs) {
			if ((StringUtils.isNotBlank(value)&&value.contains(comboBox.getValue()))
					||(StringUtils.isNotBlank(text)&&text.contains(comboBox.getName()))) {
				comboBox.setSelected(true);
			}
		}
		return comboBoxs;
	}
	
}
