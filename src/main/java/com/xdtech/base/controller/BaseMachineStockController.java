package com.xdtech.base.controller;


import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.xdtech.sys.aspect.SystemControllerLog;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.xdtech.base.service.BaseMachineStockService;
import com.xdtech.base.util.ProConstUtil;
import com.xdtech.base.vo.BaseMachineStockItem;
import com.xdtech.core.init.InitCacheData;
import com.xdtech.core.orm.utils.BeanUtils;
import com.xdtech.web.controller.BaseController;
import com.xdtech.web.model.ComboBox;
import com.xdtech.web.model.Pagination;
import com.xdtech.web.model.ResultMessage;

/**
 * 
 * @author max.zheng
 * @create 2017-03-05 12:18:45
 * @since 1.0
 * @see
 */
@Controller
@Scope("prototype")
public class BaseMachineStockController extends BaseController
{
	@Autowired
	private BaseMachineStockService baseMachineStockService;
	@RequestMapping(value="/baseMachineStock.do",params = "baseMachineStock")
	public ModelAndView skipBaseMachineStock() 
	{
		return new ModelAndView("base/baseMachineStock/baseMachineStock_ftl");
	}
	
	@RequestMapping(value="/baseMachineStock.do",params="loadList")
	@ResponseBody
	public Map<String, Object> loadList(BaseMachineStockItem item,Pagination pg) 
	{
		Map<String, Object> results =  baseMachineStockService.loadPageDataByConditions(pg,item,"findBaseMachineStockByCondition");
		return results;
	}
	
	@RequestMapping(value="/baseMachineStock.do",params="loadList2")
	@ResponseBody
	public Map<String, Object> loadList2(BaseMachineStockItem item,Pagination pg) 
	{
		item.setState(ProConstUtil.STATE_USABLE);
		Map<String, Object> results =  baseMachineStockService.loadPageDataByConditions(pg,item,"findBaseMachineStockByCondition");
		return results;
	}
	
	@RequestMapping(value="/baseMachineStock.do",params = "editBaseMachineStock")
	@SystemControllerLog(description = "编辑")
	public ModelAndView editBaseMachineStock(HttpServletRequest request,Long baseMachineStockId) 
	{
		if (baseMachineStockId!=null) 
		{
			request.setAttribute("baseMachineStockItem", baseMachineStockService.loadBaseMachineStockItem(baseMachineStockId));
		}
		return new ModelAndView("base/baseMachineStock/editBaseMachineStock_ftl");
	}
	
	@RequestMapping(value="/baseMachineStock.do",params = "saveBaseMachineStock")
	@ResponseBody
	@SystemControllerLog(description = "保存")
	public ResultMessage saveBaseMachineStock(BaseMachineStockItem item) 
	{
		ResultMessage r = new ResultMessage();
		if (baseMachineStockService.saveOrUpdateBaseMachineStock(item)) 
		{
			r.setSuccess(true);
		}
		else 
		{
			r.setSuccess(false);
		}
		return r;
	}
	
	@RequestMapping(value="/baseMachineStock.do",params = "deleteBaseMachineStockItems")
	@ResponseBody
	@SystemControllerLog(description = "删除")
	public ResultMessage deleteBaseMachineStockItems(String ids)
	{
		ResultMessage r = new ResultMessage();
		if (StringUtils.isNotEmpty(ids)) 
		{
			String[] tempIds = ids.split(",");
			List<Long> baseMachineStockIds = new ArrayList<Long>();
			for (String id : tempIds) 
			{
				baseMachineStockIds.add(Long.valueOf(id));
			}
			baseMachineStockService.deleteBaseMachineStockInfo(baseMachineStockIds);
			r.setSuccess(true);
		}
		else 
		{
			r.setSuccess(false);
		}
		return r;
	}
	
	@RequestMapping(value="/baseMachineStock.do",params = "changeBaseMachineStock")
	@ResponseBody
	@SystemControllerLog(description = "更改设备状态")
	public ResultMessage changeBaseMachineStock(String ids,String devState)
	{
		ResultMessage r = new ResultMessage();
		if (StringUtils.isNotEmpty(ids)) 
		{
			String[] tempIds = ids.split(",");
			List<Long> baseMachineStockIds = new ArrayList<Long>();
			for (String id : tempIds) 
			{
				baseMachineStockIds.add(Long.valueOf(id));
			}
			baseMachineStockService.updataMachineStockState(ids + "-1",Integer.parseInt(devState));
			r.setSuccess(true);
		}
		else 
		{
			r.setSuccess(false);
		}
		return r;
	}
	
	@RequestMapping(value="/baseMachineStock.do",params = "changeDeviceState")
	public ModelAndView skipChangeBaseMachineStock(String ids,HttpServletRequest request) 
	{
		request.setAttribute("ids", ids);
		return new ModelAndView("base/baseMachineStock/changeBaseMachineStock_ftl");
	}
	
	@RequestMapping(params = "loadComboBox")
	@ResponseBody
	public Object loadComboBox(String key,String value,String text,String isSearch) {
		List items = InitCacheData.dictionary.get(key);
		List<ComboBox> comboBoxs = BeanUtils.copyListProperties(ComboBox.class, items);
		if ("true".equals(isSearch)) {
			comboBoxs.add(0,new ComboBox("", "------", false));
		}
		for (ComboBox comboBox : comboBoxs) {
			if ((StringUtils.isNotBlank(value)&&value.contains(comboBox.getValue()))
					||(StringUtils.isNotBlank(text)&&text.contains(comboBox.getName()))) {
				comboBox.setSelected(true);
			}
		}
		return comboBoxs;
	}
	
}
