package com.xdtech.base.controller;


import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.xdtech.sys.aspect.SystemControllerLog;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.xdtech.base.service.BaseMachineBorrowService;
import com.xdtech.base.vo.BaseMachineBorrowItem;
import com.xdtech.core.init.InitCacheData;
import com.xdtech.core.orm.utils.BeanUtils;
import com.xdtech.web.controller.BaseController;
import com.xdtech.web.model.ComboBox;
import com.xdtech.web.model.Pagination;
import com.xdtech.web.model.ResultMessage;

/**
 * 
 * @author max.zheng
 * @create 2017-03-05 12:56:55
 * @since 1.0
 * @see
 */
@Controller
@Scope("prototype")
public class BaseMachineBorrowController extends BaseController
{
	@Autowired
	private BaseMachineBorrowService baseMachineBorrowService;
	@RequestMapping(value="/baseMachineBorrow.do",params = "baseMachineBorrow")
	public ModelAndView skipBaseMachineBorrow()
	{
		return new ModelAndView("base/baseMachineBorrow/baseMachineBorrow_ftl");
	}
	
	@RequestMapping(value="/baseMachineBorrow.do",params="loadList")
	@ResponseBody
	public Map<String, Object> loadList(BaseMachineBorrowItem item,Pagination pg) 
	{
		Map<String, Object> results =  baseMachineBorrowService.loadPageDataByConditions(pg,item,"findBaseMachineBorrowByCondition");
		return results;
	}
	
	@RequestMapping(value="/baseMachineBorrow.do",params = "editBaseMachineBorrow")
	@SystemControllerLog(description = "编辑")
	public ModelAndView editBaseMachineBorrow(HttpServletRequest request,Long baseMachineBorrowId) 
	{
		if (baseMachineBorrowId!=null) 
		{
			request.setAttribute("baseMachineBorrowItem", baseMachineBorrowService.loadBaseMachineBorrowItem(baseMachineBorrowId));
		}
		return new ModelAndView("base/baseMachineBorrow/editBaseMachineBorrow_ftl");
	}
	
	@RequestMapping(value="/baseMachineBorrow.do",params = "saveBaseMachineBorrow")
	@ResponseBody
	@SystemControllerLog(description = "保存")
	public ResultMessage saveBaseMachineBorrow(BaseMachineBorrowItem item)
	{
		ResultMessage r = new ResultMessage();
		if (baseMachineBorrowService.saveOrUpdateBaseMachineBorrow(item)) 
		{
			r.setSuccess(true);
		}
		else
		{
			r.setSuccess(false);
		}
		return r;
	}
	
	@RequestMapping(value="/baseMachineBorrow.do",params = "deleteBaseMachineBorrowItems")
	@ResponseBody
	@SystemControllerLog(description = "删除")
	public ResultMessage deleteBaseMachineBorrowItems(String ids)
	{
		ResultMessage r = new ResultMessage();
		if (StringUtils.isNotEmpty(ids)) 
		{
			String[] tempIds = ids.split(",");
			List<Long> baseMachineBorrowIds = new ArrayList<Long>();
			for (String id : tempIds) 
			{
				baseMachineBorrowIds.add(Long.valueOf(id));
			}
			baseMachineBorrowService.deleteBaseMachineBorrowInfo(baseMachineBorrowIds);
			r.setSuccess(true);
		}
		else 
		{
			r.setSuccess(false);
		}
		return r;
	}
	
	@RequestMapping(value="/baseMachineBorrow.do",params = "returnBaseMachineBorrowItems")
	@ResponseBody
	@SystemControllerLog(description = "归还设备")
	public ResultMessage returnBaseMachineBorrowItems(String ids)
	{
		ResultMessage r = new ResultMessage();
		if (StringUtils.isNotEmpty(ids)) 
		{
			baseMachineBorrowService.returnBaseMachineBorrow(ids + "-1");
			r.setSuccess(true);
		}
		else 
		{
			r.setSuccess(false);
		}
		return r;
	}
	
	@RequestMapping(value="/baseMachineBorrow.do",params = "remark")
	@ResponseBody
	@SystemControllerLog(description = "变更备注")
	public ResultMessage changeBaseMachineStock(String ids,String remark)
	{
		ResultMessage r = new ResultMessage();
		if (StringUtils.isNotEmpty(ids)) 
		{
			baseMachineBorrowService.updataMachineBorrowRemark(ids + "-1",remark);
			r.setSuccess(true);
		}
		else 
		{
			r.setSuccess(false);
		}
		return r;
	}
	
	@RequestMapping(value="/baseMachineBorrow.do",params = "remarkBaseMachineBorrow")
	public ModelAndView skipChangeBaseMachineStock(String ids,HttpServletRequest request) 
	{
		request.setAttribute("ids", ids);
		return new ModelAndView("base/baseMachineBorrow/remarkBaseMachineBorrow_ftl");
	}
	
	@RequestMapping(params = "loadComboBox")
	@ResponseBody
	public Object loadComboBox(String key,String value,String text,String isSearch) {
		List items = InitCacheData.dictionary.get(key);
		List<ComboBox> comboBoxs = BeanUtils.copyListProperties(ComboBox.class, items);
		if ("true".equals(isSearch)) {
			comboBoxs.add(0,new ComboBox("", "------", false));
		}
		for (ComboBox comboBox : comboBoxs) {
			if ((StringUtils.isNotBlank(value)&&value.contains(comboBox.getValue()))
					||(StringUtils.isNotBlank(text)&&text.contains(comboBox.getName()))) {
				comboBox.setSelected(true);
			}
		}
		return comboBoxs;
	}
	
}
