package com.xdtech.base.controller;


import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.xdtech.sys.aspect.SystemControllerLog;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.xdtech.base.service.BaseMachineTypeService;
import com.xdtech.base.vo.BaseMachineTypeItem;
import com.xdtech.web.controller.BaseController;
import com.xdtech.web.model.Pagination;
import com.xdtech.web.model.ResultMessage;

/**
 * 
 * @author max.zheng
 * @create 2017-03-05 12:05:53
 * @since 1.0
 * @see
 */
@Controller
@Scope("prototype")
public class BaseMachineTypeController extends BaseController
{
	@Autowired
	private BaseMachineTypeService baseMachineTypeService;
	@RequestMapping(value="/baseMachineType.do",params = "baseMachineType")
	public ModelAndView skipBaseMachineType() 
	{
		return new ModelAndView("base/baseMachineType/baseMachineType_ftl");
	}
	
	@RequestMapping(value="/baseMachineType.do",params="loadList")
	@ResponseBody
	public Map<String, Object> loadList(BaseMachineTypeItem item,Pagination pg)
	{
		Map<String, Object> results =  baseMachineTypeService.loadPageDataByConditions(pg,item,"findBaseMachineTypeByCondition");
		return results;
	}
	
	@RequestMapping(value="/baseMachineType.do",params = "editBaseMachineType")
	@SystemControllerLog(description = "编辑")
	public ModelAndView editBaseMachineType(HttpServletRequest request,Long baseMachineTypeId) 
	{
		if (baseMachineTypeId!=null) 
		{
			request.setAttribute("baseMachineTypeItem", baseMachineTypeService.loadBaseMachineTypeItem(baseMachineTypeId));
		}
		return new ModelAndView("base/baseMachineType/editBaseMachineType_ftl");
	}
	
	@RequestMapping(value="/baseMachineType.do",params = "saveBaseMachineType")
	@ResponseBody
	@SystemControllerLog(description = "保存")
	public ResultMessage saveBaseMachineType(BaseMachineTypeItem item)
	{
		ResultMessage r = new ResultMessage();
		if (baseMachineTypeService.saveOrUpdateBaseMachineType(item))
		{
			r.setSuccess(true);
		}
		else 
		{
			r.setSuccess(false);
		}
		return r;
	}
	
	@RequestMapping(value="/baseMachineType.do",params = "deleteBaseMachineTypeItems")
	@ResponseBody
	@SystemControllerLog(description = "删除")
	public ResultMessage deleteBaseMachineTypeItems(String ids) 
	{
		ResultMessage r = new ResultMessage();
		if (StringUtils.isNotEmpty(ids))
		{
			String[] tempIds = ids.split(",");
			List<Long> baseMachineTypeIds = new ArrayList<Long>();
			for (String id : tempIds) 
			{
				baseMachineTypeIds.add(Long.valueOf(id));
			}
			baseMachineTypeService.deleteBaseMachineTypeInfo(baseMachineTypeIds);
			r.setSuccess(true);
		}
		else 
		{
			r.setSuccess(false);
		}
		return r;
	}
}
