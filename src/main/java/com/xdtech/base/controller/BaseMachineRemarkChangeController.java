package com.xdtech.base.controller;


import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.xdtech.sys.aspect.SystemControllerLog;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.xdtech.base.service.BaseMachineRemarkChangeService;
import com.xdtech.base.vo.BaseMachineRemarkChangeItem;
import com.xdtech.web.controller.BaseController;
import com.xdtech.web.model.Pagination;
import com.xdtech.web.model.ResultMessage;

/**
 * 
 * @author max.zheng
 * @create 2017-03-05 13:19:30
 * @since 1.0
 * @see
 */
@Controller
@Scope("prototype")
public class BaseMachineRemarkChangeController extends BaseController
{
	@Autowired
	private BaseMachineRemarkChangeService baseMachineRemarkChangeService;
	@RequestMapping(value="/baseMachineRemarkChange.do",params = "baseMachineRemarkChange")
	public ModelAndView skipBaseMachineRemarkChange() 
	{
		return new ModelAndView("base/baseMachineRemarkChange/baseMachineRemarkChange_ftl");
	}
	
	@RequestMapping(value="/baseMachineRemarkChange.do",params="loadList")
	@ResponseBody
	public Map<String, Object> loadList(BaseMachineRemarkChangeItem item,Pagination pg)
	{
		Map<String, Object> results =  baseMachineRemarkChangeService.loadPageDataByConditions(pg,item,"findBaseMachineRemarkChangeByCondition");
		return results;
	}
	
	@RequestMapping(value="/baseMachineRemarkChange.do",params = "editBaseMachineRemarkChange")
	@SystemControllerLog(description = "编辑")
	public ModelAndView editBaseMachineRemarkChange(HttpServletRequest request,Long baseMachineRemarkChangeId) 
	{
		if (baseMachineRemarkChangeId!=null) 
		{
			request.setAttribute("baseMachineRemarkChangeItem", baseMachineRemarkChangeService.loadBaseMachineRemarkChangeItem(baseMachineRemarkChangeId));
		}
		return new ModelAndView("base/baseMachineRemarkChange/editBaseMachineRemarkChange_ftl");
	}
	
	@RequestMapping(value="/baseMachineRemarkChange.do",params = "saveBaseMachineRemarkChange")
	@ResponseBody
	@SystemControllerLog(description = "保存")
	public ResultMessage saveBaseMachineRemarkChange(BaseMachineRemarkChangeItem item) 
	{
		ResultMessage r = new ResultMessage();
		if (baseMachineRemarkChangeService.saveOrUpdateBaseMachineRemarkChange(item)) 
		{
			r.setSuccess(true);
		}
		else
		{
			r.setSuccess(false);
		}
		return r;
	}
	
	@RequestMapping(value="/baseMachineRemarkChange.do",params = "deleteBaseMachineRemarkChangeItems")
	@ResponseBody
	@SystemControllerLog(description = "删除")
	public ResultMessage deleteBaseMachineRemarkChangeItems(String ids) 
	{
		ResultMessage r = new ResultMessage();
		if (StringUtils.isNotEmpty(ids)) 
		{
			String[] tempIds = ids.split(",");
			List<Long> baseMachineRemarkChangeIds = new ArrayList<Long>();
			for (String id : tempIds) 
			{
				baseMachineRemarkChangeIds.add(Long.valueOf(id));
			}
			baseMachineRemarkChangeService.deleteBaseMachineRemarkChangeInfo(baseMachineRemarkChangeIds);
			r.setSuccess(true);
		}
		else 
		{
			r.setSuccess(false);
		}
		return r;
	}
}
