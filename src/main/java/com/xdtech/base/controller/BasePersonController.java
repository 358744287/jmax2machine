package com.xdtech.base.controller;


import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.xdtech.sys.aspect.SystemControllerLog;
import com.xdtech.sys.model.User;
import com.xdtech.sys.service.UserService;
import com.xdtech.sys.util.SessionContextUtil;
import com.xdtech.sys.vo.UserItem;

import org.apache.commons.lang.StringUtils;
import org.hibernate.hql.ast.tree.IsNotNullLogicOperatorNode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.xdtech.base.model.BasePerson;
import com.xdtech.base.service.BasePersonService;
import com.xdtech.base.vo.BasePersonItem;
import com.xdtech.web.controller.BaseController;
import com.xdtech.web.model.Pagination;
import com.xdtech.web.model.ResultMessage;

/**
 * 
 * @author max.zheng
 * @create 2017-03-05 11:52:17
 * @since 1.0
 * @see
 */
@Controller
@Scope("prototype")
public class BasePersonController extends BaseController
{
	@Autowired
	private BasePersonService basePersonService;
	@Autowired
	private UserService userService;
	@RequestMapping(value="/basePerson.do",params = "basePerson")
	public ModelAndView skipBasePerson()
	{
		return new ModelAndView("base/basePerson/basePerson_ftl");
	}
	
	@RequestMapping(value="/basePerson.do",params="loadList")
	@ResponseBody
	public Map<String, Object> loadList(BasePersonItem item,Pagination pg) 
	{
		Map<String, Object> results =  basePersonService.loadPageDataByConditions(pg,item,"findBasePersonByCondition");
		return results;
	}
	
	@RequestMapping(value="/basePerson.do",params = "editBasePerson")
	@SystemControllerLog(description = "编辑")
	public ModelAndView editBasePerson(HttpServletRequest request,Long basePersonId) 
	{
		if (basePersonId!=null) 
		{
			request.setAttribute("basePersonItem", basePersonService.loadBasePersonItem(basePersonId));
		}
		UserItem user = SessionContextUtil.getCurrentUser();
		request.setAttribute("currentUser", user);
		return new ModelAndView("base/basePerson/editBasePerson_ftl");
	}
	
	@RequestMapping(value="/basePerson.do",params = "saveBasePerson")
	@ResponseBody
	@SystemControllerLog(description = "保存")
	public ResultMessage saveBasePerson(BasePersonItem item) 
	{
		ResultMessage r = new ResultMessage();
		if (basePersonService.saveOrUpdateBasePerson(item)) 
		{
			r.setSuccess(true);
		}
		else 
		{
			r.setSuccess(false);
		}
		return r;
	}
	
	@RequestMapping(value="/basePerson.do",params = "deleteBasePersonItems")
	@ResponseBody
	@SystemControllerLog(description = "删除")
	public ResultMessage deleteBasePersonItems(String ids) 
	{
		ResultMessage r = new ResultMessage();
		if (StringUtils.isNotEmpty(ids))
		{
			String[] tempIds = ids.split(",");
			List<Long> basePersonIds = new ArrayList<Long>();
			for (String id : tempIds)
			{
				basePersonIds.add(Long.valueOf(id));
			}
			basePersonService.deleteBasePersonInfo(basePersonIds);
			r.setSuccess(true);
		}
		else
		{
			r.setSuccess(false);
		}
		return r;
	}
	
	
	@RequestMapping(value="/basePerson.do",params = "createSysUser")
	@ResponseBody
	@SystemControllerLog(description = "创建系统用户")
	public ResultMessage createSysUser(String ids) 
	{
		//判断是否成功的值
		ResultMessage r = new ResultMessage();
		//获取人员id的值
		String[] idList = ids.split(",");
		r.setSuccess(basePersonService.createUsers(idList));
		return r;
	}
}
