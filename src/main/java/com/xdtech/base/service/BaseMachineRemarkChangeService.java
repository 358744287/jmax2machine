package com.xdtech.base.service;

import java.util.List;

import com.xdtech.common.service.IBaseService;
import com.xdtech.base.model.BaseMachineRemarkChange;
import com.xdtech.base.vo.BaseMachineRemarkChangeItem;

/**
 * 
 * @author max.zheng
 * @create 2017-03-05 13:19:30
 * @since 1.0
 * @see
 */
public interface BaseMachineRemarkChangeService extends IBaseService<BaseMachineRemarkChange>{

	/**
	 * 保存更新信息
	 * @author max.zheng
	 * @create 2017-03-05 13:19:30
	 * @modified by
	 * @param item
	 * @return
	 */
	boolean saveOrUpdateBaseMachineRemarkChange(BaseMachineRemarkChangeItem item);

	/**
	 * 加载记录信息
	 * @author max.zheng
	 * @create 2017-03-05 13:19:30
	 * @modified by
	 * @param newId
	 * @return
	 */
	BaseMachineRemarkChangeItem loadBaseMachineRemarkChangeItem(Long baseMachineRemarkChangeId);

	/**
	 * 根据id号删除记录信息
	 * @author max.zheng
	 * @create 2017-03-05 13:19:30
	 * @modified by
	 * @param id
	 * @return
	 */
	boolean deleteBaseMachineRemarkChangeInfo(long id);

	/**
	 * 
	 * @author max.zheng
	 * @create 2017-03-05 13:19:30
	 * @modified by
	 * @param baseMachineRemarkChangeIds
	 */
	boolean deleteBaseMachineRemarkChangeInfo(List<Long> baseMachineRemarkChangeIds);
	
	/**
	 * 加载vo列表条目
	 * @author max.zheng
	 * @create 2017-03-05 13:19:30
	 * @modified by
	 * @return
	 */
	List<BaseMachineRemarkChangeItem> loadItems();
	/**
	 * 根据查询器直接查询符合的数据集合，不分页
	 * @author max.zheng
	 * @create 2017-03-05 13:19:30
	 * @modified by
	 * @return
	 */
	public List<BaseMachineRemarkChangeItem> getBaseMachineRemarkChangeByCondition(
			BaseMachineRemarkChangeItem condition);
}
