package com.xdtech.base.service;

import java.util.List;

import com.xdtech.common.service.IBaseService;
import com.xdtech.base.model.BasePerson;
import com.xdtech.base.vo.BasePersonItem;

/**
 * 
 * @author max.zheng
 * @create 2017-03-05 11:52:17
 * @since 1.0
 * @see
 */
public interface BasePersonService extends IBaseService<BasePerson>{

	/**
	 * 保存更新信息
	 * @author max.zheng
	 * @create 2017-03-05 11:52:17
	 * @modified by
	 * @param item
	 * @return
	 */
	boolean saveOrUpdateBasePerson(BasePersonItem item);

	/**
	 * 加载记录信息
	 * @author max.zheng
	 * @create 2017-03-05 11:52:17
	 * @modified by
	 * @param newId
	 * @return
	 */
	BasePersonItem loadBasePersonItem(Long basePersonId);

	/**
	 * 根据id号删除记录信息
	 * @author max.zheng
	 * @create 2017-03-05 11:52:17
	 * @modified by
	 * @param id
	 * @return
	 */
	boolean deleteBasePersonInfo(long id);

	/**
	 * 
	 * @author max.zheng
	 * @create 2017-03-05 11:52:17
	 * @modified by
	 * @param basePersonIds
	 */
	boolean deleteBasePersonInfo(List<Long> basePersonIds);
	
	/**
	 * 加载vo列表条目
	 * @author max.zheng
	 * @create 2017-03-05 11:52:17
	 * @modified by
	 * @return
	 */
	List<BasePersonItem> loadItems();
	/**
	 * 根据查询器直接查询符合的数据集合，不分页
	 * @author max.zheng
	 * @create 2017-03-05 11:52:17
	 * @modified by
	 * @return
	 */
	public List<BasePersonItem> getBasePersonByCondition(
			BasePersonItem condition);
	
	/**
	 * 人员关联系统用户通过userId
	 * 
	 */
	boolean createSysUser(BasePersonItem linkSysUser);

	boolean createUsers(String[] idList);
	
	
}
