package com.xdtech.base.service;

import java.util.List;

import com.xdtech.common.service.IBaseService;
import com.xdtech.base.model.BaseMachineRequestForm;
import com.xdtech.base.model.BaseMachineStock;
import com.xdtech.base.vo.BaseMachineStockItem;

/**
 * 
 * @author max.zheng
 * @create 2017-03-05 12:18:45
 * @since 1.0
 * @see
 */
public interface BaseMachineStockService extends IBaseService<BaseMachineStock>{

	/**
	 * 保存更新信息
	 * @author max.zheng
	 * @create 2017-03-05 12:18:45
	 * @modified by
	 * @param item
	 * @return
	 */
	boolean saveOrUpdateBaseMachineStock(BaseMachineStockItem item);

	/**
	 * 加载记录信息
	 * @author max.zheng
	 * @create 2017-03-05 12:18:45
	 * @modified by
	 * @param newId
	 * @return
	 */
	BaseMachineStockItem loadBaseMachineStockItem(Long baseMachineStockId);

	/**
	 * 根据id号删除记录信息
	 * @author max.zheng
	 * @create 2017-03-05 12:18:45
	 * @modified by
	 * @param id
	 * @return
	 */
	boolean deleteBaseMachineStockInfo(long id);

	/**
	 * 
	 * @author max.zheng
	 * @create 2017-03-05 12:18:45
	 * @modified by
	 * @param baseMachineStockIds
	 */
	boolean deleteBaseMachineStockInfo(List<Long> baseMachineStockIds);
	
	/**
	 * 加载vo列表条目
	 * @author max.zheng
	 * @create 2017-03-05 12:18:45
	 * @modified by
	 * @return
	 */
	List<BaseMachineStockItem> loadItems();
	/**
	 * 根据查询器直接查询符合的数据集合，不分页
	 * @author max.zheng
	 * @create 2017-03-05 12:18:45
	 * @modified by
	 * @return
	 */
	public List<BaseMachineStockItem> getBaseMachineStockByCondition(
			BaseMachineStockItem condition);

	public boolean updataMachineStockState(String devIds,Integer devState);
	
	public boolean updataMachineStockState(BaseMachineRequestForm baseMachineRequestForm,Integer state);
	
	public List<BaseMachineStock> findByDevIds(String devIds);

	public List<BaseMachineStock> findByOrderNos(String orderNos);
}
