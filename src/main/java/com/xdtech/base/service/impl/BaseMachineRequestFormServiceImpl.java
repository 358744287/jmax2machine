package com.xdtech.base.service.impl;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xdtech.base.dao.BaseMachineRequestFormDao;
import com.xdtech.base.model.BaseMachineRequestForm;
import com.xdtech.base.model.BaseMachineStock;
import com.xdtech.base.service.BaseMachineRequestFormService;
import com.xdtech.base.service.BaseMachineStockService;
import com.xdtech.base.util.ProConstUtil;
import com.xdtech.base.vo.BaseMachineRequestFormItem;
import com.xdtech.common.utils.DateUtil;
import com.xdtech.core.dao.BaseDao;
import com.xdtech.core.model.BaseItem;
import com.xdtech.core.orm.Page;
import com.xdtech.core.orm.utils.BeanUtils;
import com.xdtech.web.model.Pagination;

/**
 * 
 * @author max.zheng
 * @create 2017-03-05 12:36:02
 * @since 1.0
 * @see
 */
@Service
public class BaseMachineRequestFormServiceImpl implements BaseMachineRequestFormService {
	private Log log = LogFactory.getLog(BaseMachineRequestFormServiceImpl.class);
	@Autowired
	private BaseMachineRequestFormDao baseMachineRequestFormDao;
	@Autowired
	private BaseDao baseDao;
	@Autowired
	private BaseMachineStockService baseMachineStockService;
	/**
	 * @description
	 * @author max.zheng
	 * @create 2017-03-05 12:36:02
	 * @modified by
	 * @param entity
	 */
	public void save(BaseMachineRequestForm entity) {
		baseMachineRequestFormDao.save(entity);
	}
	/**
	 * @description
	 * @author max.zheng
	 * @create 2017-03-05 12:36:02
	 * @modified by
	 * @param entity
	 */
	public void saveAll(List<BaseMachineRequestForm> entities) {
		baseMachineRequestFormDao.saveAll(entities);
	}

	/**
	 * @description
	 * @author max.zheng
	 * @create 2017-03-05 12:36:02
	 * @modified by
	 * @param entity
	 */
	public void delete(BaseMachineRequestForm entity) {
		baseMachineRequestFormDao.delete(entity);
	}

	/**
	 * @description
	 * @author max.zheng
	 * @create 2017-03-05 12:36:02
	 * @modified by
	 * @param id
	 */
	public void delete(Long id) {
		baseMachineRequestFormDao.delete(id);
	}

	/**
	 * @description
	 * @author max.zheng
	 * @create 2017-03-05 12:36:02
	 * @modified by
	 * @param id
	 * @return
	 */
	public BaseMachineRequestForm get(Long id) {
		return baseMachineRequestFormDao.get(id);
	}

	/**
	 * @description
	 * @author max.zheng
	 * @create 2017-03-05 12:36:02
	 * @modified by
	 * @return
	 */
	public List<BaseMachineRequestForm> getAll() {
		return baseMachineRequestFormDao.getAll();
	}

	/**
	 * @description
	 * @author max.zheng
	 * @create 2017-03-05 12:36:02
	 * @modified by
	 * @param pg
	 * @param values
	 * @return
	 */
	public Map<String, Object> loadPageAndCondition(Pagination pg,
			Map<String, String> values) {
		Map<String, Object> results = new HashMap<String, Object>();
		List<Object> baseMachineRequestForms = null;
		long rows = 0;
		if (pg!=null) {
			Page<BaseMachineRequestForm> page = baseMachineRequestFormDao.findPage(pg,"from BaseMachineRequestForm order by createTime desc", values);
			baseMachineRequestForms = BeanUtils.copyListProperties(
					BaseMachineRequestFormItem.class, page.getResult());
			rows = page.getTotalItems();
		}else {
			List<BaseMachineRequestForm> baseMachineRequestFormList = baseMachineRequestFormDao.find("from BaseMachineRequestForm order by id desc", values);
			baseMachineRequestForms = BeanUtils.copyListProperties(
					BaseMachineRequestFormItem.class, baseMachineRequestFormList);
			rows = baseMachineRequestForms.size();
		}
		results.put("total", rows);
		results.put("rows", baseMachineRequestForms);
		return results;
	}

	/**
	 * @description
	 * @author max.zheng
	 * @create 2017-03-05 12:36:02
	 * @modified by
	 * @param item
	 * @return
	 */
	public boolean saveOrUpdateBaseMachineRequestForm(BaseMachineRequestFormItem item) {
		BaseMachineRequestForm baseMachineRequestForm = null;
		if (item.getId()==null) {
			item.setRequestDate(DateUtil.dateToString(new Date(), "yyyy-MM-dd HH:mm:ss"));
			item.setRequestState(ProConstUtil.RESPONSE_NO);
			baseMachineRequestForm = new BaseMachineRequestForm();
		}else {
			baseMachineRequestForm = baseMachineRequestFormDao.get(item.getId());
		}
		String devContent = "";
		if(StringUtils.isNotEmpty(item.getDevIds()))
		{
			String [] devId = item.getDevIds().split(",");
			for (String id : devId)
			{
				BaseMachineStock baseMachineStock = baseMachineStockService.get(Long.parseLong(id));
				devContent = devContent + baseMachineStock.getOrderNo() + " " + baseMachineStock.getBaseMachineType().getType() + ",";
			}
		}
		//复制前台修改的属性
		BeanUtils.copyProperties(baseMachineRequestForm, item);
		baseMachineRequestForm.setRequestContent(devContent);
		baseMachineRequestFormDao.save(baseMachineRequestForm);
		return true;
	}

	/**
	 * @description
	 * @author max.zheng
	 * @create 2017-03-05 12:36:02
	 * @modified by
	 * @param baseMachineRequestFormId
	 * @return
	 */
	public BaseMachineRequestFormItem loadBaseMachineRequestFormItem(Long baseMachineRequestFormId) {
		BaseMachineRequestForm baseMachineRequestForm = baseMachineRequestFormDao.get(baseMachineRequestFormId);
		return BaseMachineRequestFormItem.createItem(baseMachineRequestForm);
	}

	/**
	 * @description
	 * @author max.zheng
	 * @create 2017-03-05 12:36:02
	 * @modified by
	 * @param id
	 * @return
	 */
	public boolean deleteBaseMachineRequestFormInfo(long id) {
		delete(id);
		return true;
	}
	
	/**
	 * @description
	 * @author max.zheng
	 * @create 2017-03-05 12:36:02
	 * @modified by
	 * @param newIds
	 * @return
	 */
	public boolean deleteBaseMachineRequestFormInfo(List<Long> baseMachineRequestFormIds) {
		for (Long id : baseMachineRequestFormIds) {
			delete(id);
		}
		return true;
	}
	
	public Map<String, Object> loadPageDataByConditions(Pagination pg,
			BaseItem baseItem, String queryName) {
		Map<String, Object> results = new HashMap<String, Object>();
		Page page = baseDao.findPageByNamedQuery(pg, queryName,baseItem);
		results.put("total", page.getTotalItems());
		results.put("rows", page.getResult());
		return results;
	}
	/**
	 * @description
	 * @author max.zheng
	 * @create 2017-03-05 12:36:02
	 * @modified by
	 * @return
	 */
	@Override
	public List<BaseMachineRequestFormItem> loadItems() {
		List<BaseMachineRequestForm> baseMachineRequestForms = getAll();
		return BaseMachineRequestFormItem.createItems(baseMachineRequestForms);
	}
	
	public List<BaseMachineRequestFormItem> getBaseMachineRequestFormByCondition(
			BaseMachineRequestFormItem condition) {
		List<BaseMachineRequestFormItem> items = baseDao.findByNamedQuery("findBaseMachineRequestFormByCondition",condition,condition.loadQueryFields());
		return items;
	}
	public boolean passMachineRequestForm(String baseMachineRequestFormIds)
	{
		List<BaseMachineRequestForm> baseMachineRequestForms = baseMachineRequestFormDao.findByHql("from BaseMachineRequestForm e where e.id in (" + baseMachineRequestFormIds +")");
		for (BaseMachineRequestForm baseMachineRequestForm : baseMachineRequestForms)
		{
			baseMachineStockService.updataMachineStockState(baseMachineRequestForm,ProConstUtil.STATE_LOAN);
			baseMachineRequestForm.setRequestState(ProConstUtil.RESPONSE_YES);
			baseMachineRequestForm.setResponseDate(DateUtil.dateToString(new Date(), "yyyy-MM-dd hh:mm:ss"));
			baseMachineRequestFormDao.save(baseMachineRequestForm);
		}
		return true;
	}

}
