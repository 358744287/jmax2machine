package com.xdtech.base.service.impl;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.xdtech.core.dao.BaseDao;
import com.xdtech.core.model.BaseItem;
import com.xdtech.core.orm.Page;
import com.xdtech.core.orm.utils.BeanUtils;
import com.xdtech.base.dao.BaseMachineTypeDao;
import com.xdtech.base.model.BaseMachineType;
import com.xdtech.base.service.BaseMachineTypeService;
import com.xdtech.base.vo.BaseMachineTypeItem;
import com.xdtech.web.model.Pagination;

/**
 * 
 * @author max.zheng
 * @create 2017-03-05 12:05:53
 * @since 1.0
 * @see
 */
@Service
public class BaseMachineTypeServiceImpl implements BaseMachineTypeService {
	private Log log = LogFactory.getLog(BaseMachineTypeServiceImpl.class);
	@Autowired
	private BaseMachineTypeDao baseMachineTypeDao;
	@Autowired
	private BaseDao baseDao;
	/**
	 * @description
	 * @author max.zheng
	 * @create 2017-03-05 12:05:53
	 * @modified by
	 * @param entity
	 */
	public void save(BaseMachineType entity) {
		baseMachineTypeDao.save(entity);
	}
	/**
	 * @description
	 * @author max.zheng
	 * @create 2017-03-05 12:05:53
	 * @modified by
	 * @param entity
	 */
	public void saveAll(List<BaseMachineType> entities) {
		baseMachineTypeDao.saveAll(entities);
	}

	/**
	 * @description
	 * @author max.zheng
	 * @create 2017-03-05 12:05:53
	 * @modified by
	 * @param entity
	 */
	public void delete(BaseMachineType entity) {
		baseMachineTypeDao.delete(entity);
	}

	/**
	 * @description
	 * @author max.zheng
	 * @create 2017-03-05 12:05:53
	 * @modified by
	 * @param id
	 */
	public void delete(Long id) {
		baseMachineTypeDao.delete(id);
	}

	/**
	 * @description
	 * @author max.zheng
	 * @create 2017-03-05 12:05:53
	 * @modified by
	 * @param id
	 * @return
	 */
	public BaseMachineType get(Long id) {
		return baseMachineTypeDao.get(id);
	}

	/**
	 * @description
	 * @author max.zheng
	 * @create 2017-03-05 12:05:53
	 * @modified by
	 * @return
	 */
	public List<BaseMachineType> getAll() {
		return baseMachineTypeDao.getAll();
	}

	/**
	 * @description
	 * @author max.zheng
	 * @create 2017-03-05 12:05:53
	 * @modified by
	 * @param pg
	 * @param values
	 * @return
	 */
	public Map<String, Object> loadPageAndCondition(Pagination pg,
			Map<String, String> values) {
		Map<String, Object> results = new HashMap<String, Object>();
		List<Object> baseMachineTypes = null;
		long rows = 0;
		if (pg!=null) {
			Page<BaseMachineType> page = baseMachineTypeDao.findPage(pg,"from BaseMachineType order by createTime desc", values);
			baseMachineTypes = BeanUtils.copyListProperties(
					BaseMachineTypeItem.class, page.getResult());
			rows = page.getTotalItems();
		}else {
			List<BaseMachineType> baseMachineTypeList = baseMachineTypeDao.find("from BaseMachineType order by id desc", values);
			baseMachineTypes = BeanUtils.copyListProperties(
					BaseMachineTypeItem.class, baseMachineTypeList);
			rows = baseMachineTypes.size();
		}
		results.put("total", rows);
		results.put("rows", baseMachineTypes);
		return results;
	}

	/**
	 * @description
	 * @author max.zheng
	 * @create 2017-03-05 12:05:53
	 * @modified by
	 * @param item
	 * @return
	 */
	public boolean saveOrUpdateBaseMachineType(BaseMachineTypeItem item) {
		BaseMachineType baseMachineType = null;
		if (item.getId()==null) {
			baseMachineType = new BaseMachineType();
		}else {
			baseMachineType = baseMachineTypeDao.get(item.getId());
		}
		//复制前台修改的属性
		BeanUtils.copyProperties(baseMachineType, item);
		baseMachineTypeDao.save(baseMachineType);
		return true;
	}

	/**
	 * @description
	 * @author max.zheng
	 * @create 2017-03-05 12:05:53
	 * @modified by
	 * @param baseMachineTypeId
	 * @return
	 */
	public BaseMachineTypeItem loadBaseMachineTypeItem(Long baseMachineTypeId) {
		BaseMachineType baseMachineType = baseMachineTypeDao.get(baseMachineTypeId);
		return BaseMachineTypeItem.createItem(baseMachineType);
	}

	/**
	 * @description
	 * @author max.zheng
	 * @create 2017-03-05 12:05:53
	 * @modified by
	 * @param id
	 * @return
	 */
	public boolean deleteBaseMachineTypeInfo(long id) {
		delete(id);
		return true;
	}
	
	/**
	 * @description
	 * @author max.zheng
	 * @create 2017-03-05 12:05:53
	 * @modified by
	 * @param newIds
	 * @return
	 */
	public boolean deleteBaseMachineTypeInfo(List<Long> baseMachineTypeIds) {
		for (Long id : baseMachineTypeIds) {
			delete(id);
		}
		return true;
	}
	
	public Map<String, Object> loadPageDataByConditions(Pagination pg,
			BaseItem baseItem, String queryName) {
		Map<String, Object> results = new HashMap<String, Object>();
		Page page = baseDao.findPageByNamedQuery(pg, queryName,baseItem);
		results.put("total", page.getTotalItems());
		results.put("rows", page.getResult());
		return results;
	}
	/**
	 * @description
	 * @author max.zheng
	 * @create 2017-03-05 12:05:53
	 * @modified by
	 * @return
	 */
	@Override
	public List<BaseMachineTypeItem> loadItems() {
		List<BaseMachineType> baseMachineTypes = getAll();
		return BaseMachineTypeItem.createItems(baseMachineTypes);
	}
	
	public List<BaseMachineTypeItem> getBaseMachineTypeByCondition(
			BaseMachineTypeItem condition) {
		List<BaseMachineTypeItem> items = baseDao.findByNamedQuery("findBaseMachineTypeByCondition",condition,condition.loadQueryFields());
		return items;
	}

}
