package com.xdtech.base.service.impl;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xdtech.base.dao.BaseMachineBorrowDao;
import com.xdtech.base.model.BaseMachineBorrow;
import com.xdtech.base.model.BaseMachineRemarkChange;
import com.xdtech.base.model.BaseMachineStock;
import com.xdtech.base.service.BaseMachineBorrowService;
import com.xdtech.base.service.BaseMachineRemarkChangeService;
import com.xdtech.base.service.BaseMachineStockService;
import com.xdtech.base.util.ProConstUtil;
import com.xdtech.base.vo.BaseMachineBorrowItem;
import com.xdtech.common.utils.DateUtil;
import com.xdtech.core.dao.BaseDao;
import com.xdtech.core.model.BaseItem;
import com.xdtech.core.orm.Page;
import com.xdtech.core.orm.utils.BeanUtils;
import com.xdtech.web.model.Pagination;

/**
 * 
 * @author max.zheng
 * @create 2017-03-05 12:56:55
 * @since 1.0
 * @see
 */
@Service
public class BaseMachineBorrowServiceImpl implements BaseMachineBorrowService {
	private Log log = LogFactory.getLog(BaseMachineBorrowServiceImpl.class);
	@Autowired
	private BaseMachineBorrowDao baseMachineBorrowDao;
	@Autowired
	private BaseDao baseDao;
	@Autowired
	private BaseMachineStockService baseMachineStockService;
	@Autowired
	private BaseMachineRemarkChangeService baseMachineRemarkChangeService;
	/**
	 * @description
	 * @author max.zheng
	 * @create 2017-03-05 12:56:55
	 * @modified by
	 * @param entity
	 */
	public void save(BaseMachineBorrow entity) {
		baseMachineBorrowDao.save(entity);
	}
	/**
	 * @description
	 * @author max.zheng
	 * @create 2017-03-05 12:56:55
	 * @modified by
	 * @param entity
	 */
	public void saveAll(List<BaseMachineBorrow> entities) {
		baseMachineBorrowDao.saveAll(entities);
	}

	/**
	 * @description
	 * @author max.zheng
	 * @create 2017-03-05 12:56:55
	 * @modified by
	 * @param entity
	 */
	public void delete(BaseMachineBorrow entity) {
		baseMachineBorrowDao.delete(entity);
	}

	/**
	 * @description
	 * @author max.zheng
	 * @create 2017-03-05 12:56:55
	 * @modified by
	 * @param id
	 */
	public void delete(Long id) {
		baseMachineBorrowDao.delete(id);
	}

	/**
	 * @description
	 * @author max.zheng
	 * @create 2017-03-05 12:56:55
	 * @modified by
	 * @param id
	 * @return
	 */
	public BaseMachineBorrow get(Long id) {
		return baseMachineBorrowDao.get(id);
	}

	/**
	 * @description
	 * @author max.zheng
	 * @create 2017-03-05 12:56:55
	 * @modified by
	 * @return
	 */
	public List<BaseMachineBorrow> getAll() {
		return baseMachineBorrowDao.getAll();
	}

	/**
	 * @description
	 * @author max.zheng
	 * @create 2017-03-05 12:56:55
	 * @modified by
	 * @param pg
	 * @param values
	 * @return
	 */
	public Map<String, Object> loadPageAndCondition(Pagination pg,
			Map<String, String> values) {
		Map<String, Object> results = new HashMap<String, Object>();
		List<Object> baseMachineBorrows = null;
		long rows = 0;
		if (pg!=null) {
			Page<BaseMachineBorrow> page = baseMachineBorrowDao.findPage(pg,"from BaseMachineBorrow order by createTime desc", values);
			baseMachineBorrows = BeanUtils.copyListProperties(
					BaseMachineBorrowItem.class, page.getResult());
			rows = page.getTotalItems();
		}else {
			List<BaseMachineBorrow> baseMachineBorrowList = baseMachineBorrowDao.find("from BaseMachineBorrow order by id desc", values);
			baseMachineBorrows = BeanUtils.copyListProperties(
					BaseMachineBorrowItem.class, baseMachineBorrowList);
			rows = baseMachineBorrows.size();
		}
		results.put("total", rows);
		results.put("rows", baseMachineBorrows);
		return results;
	}

	/**
	 * @description
	 * @author max.zheng
	 * @create 2017-03-05 12:56:55
	 * @modified by
	 * @param item
	 * @return
	 */
	public boolean saveOrUpdateBaseMachineBorrow(BaseMachineBorrowItem item) {
		BaseMachineBorrow baseMachineBorrow = null;
		if (item.getId()==null) {
			baseMachineBorrow = new BaseMachineBorrow();
		}else {
			baseMachineBorrow = baseMachineBorrowDao.get(item.getId());
		}
		//复制前台修改的属性
		BeanUtils.copyProperties(baseMachineBorrow, item);
		baseMachineBorrowDao.save(baseMachineBorrow);
		return true;
	}

	/**
	 * @description
	 * @author max.zheng
	 * @create 2017-03-05 12:56:55
	 * @modified by
	 * @param baseMachineBorrowId
	 * @return
	 */
	public BaseMachineBorrowItem loadBaseMachineBorrowItem(Long baseMachineBorrowId) {
		BaseMachineBorrow baseMachineBorrow = baseMachineBorrowDao.get(baseMachineBorrowId);
		return BaseMachineBorrowItem.createItem(baseMachineBorrow);
	}

	/**
	 * @description
	 * @author max.zheng
	 * @create 2017-03-05 12:56:55
	 * @modified by
	 * @param id
	 * @return
	 */
	public boolean deleteBaseMachineBorrowInfo(long id) {
		delete(id);
		return true;
	}
	
	/**
	 * @description
	 * @author max.zheng
	 * @create 2017-03-05 12:56:55
	 * @modified by
	 * @param newIds
	 * @return
	 */
	public boolean deleteBaseMachineBorrowInfo(List<Long> baseMachineBorrowIds) {
		for (Long id : baseMachineBorrowIds) {
			delete(id);
		}
		return true;
	}
	
	public Map<String, Object> loadPageDataByConditions(Pagination pg,
			BaseItem baseItem, String queryName) {
		Map<String, Object> results = new HashMap<String, Object>();
		Page page = baseDao.findPageByNamedQuery(pg, queryName,baseItem);
		results.put("total", page.getTotalItems());
		results.put("rows", page.getResult());
		return results;
	}
	/**
	 * @description
	 * @author max.zheng
	 * @create 2017-03-05 12:56:55
	 * @modified by
	 * @return
	 */
	@Override
	public List<BaseMachineBorrowItem> loadItems() {
		List<BaseMachineBorrow> baseMachineBorrows = getAll();
		return BaseMachineBorrowItem.createItems(baseMachineBorrows);
	}
	
	public List<BaseMachineBorrowItem> getBaseMachineBorrowByCondition(
			BaseMachineBorrowItem condition) {
		List<BaseMachineBorrowItem> items = baseDao.findByNamedQuery("findBaseMachineBorrowByCondition",condition,condition.loadQueryFields());
		return items;
	}

	@Override
	public void returnBaseMachineBorrow(String ids)
	{
    	List<BaseMachineBorrow> borrowList = baseMachineBorrowDao.findByHql(String.format("from BaseMachineBorrow b where b.isReturn=%s and b.id in (%s)", 0, ids));
    	StringBuffer devIdsStr = new StringBuffer();
    	StringBuffer orderNoStr = new StringBuffer();
    	for(BaseMachineBorrow borrow : borrowList)
    	{
    		borrow.setReturnDate(DateUtil.dateToString(new Date(),"yyyy-MM-dd hh:mm:ss"));
    		borrow.setIsReturn(1);
    		baseMachineBorrowDao.save(borrow);
    		if(borrow.getDevId() == null)// 导入数据，没有devId，通过固编判断
    		{
    			orderNoStr.append("'"+borrow.getOrderNo() + "',");
    		}
    		else
    		{
    			devIdsStr.append(borrow.getDevId() + ",");
    		}
    	}
    	
    	if(devIdsStr.length() > 0)
    	{
    		String devIds = devIdsStr.toString().substring(0, devIdsStr.length() -1);
    		List<BaseMachineStock> devList = baseMachineStockService.findByDevIds(devIds);
        	for(BaseMachineStock dev : devList)
        	{
        		dev.setState(ProConstUtil.STATE_USABLE);
        		baseMachineStockService.save(dev);
        	}
    	}
    	if(orderNoStr.length() > 0)
    	{
    		String orderNos = orderNoStr.toString().substring(0, orderNoStr.length() -1);
    		List<BaseMachineStock> devList = baseMachineStockService.findByOrderNos(orderNos);
        	for(BaseMachineStock dev : devList)
        	{
        		dev.setState(ProConstUtil.STATE_USABLE);
        		baseMachineStockService.save(dev);
        	}
    	}
	}
	@Override
	public void updataMachineBorrowRemark(String ids, String remark)
	{
		List<BaseMachineBorrow> borrowList = baseMachineBorrowDao.findByHql("from BaseMachineBorrow where id in (" + ids + ")");
		BaseMachineRemarkChange rc = null;
		for(BaseMachineBorrow b : borrowList)
		{
			rc = new BaseMachineRemarkChange();
			rc.setDevSn(b.getDevSn());
			rc.setDevName(b.getDevName());
			rc.setOrderNo(b.getOrderNo());
			rc.setRemark(b.getRemark());
			rc.setRemarkChange(remark);
			rc.setChangeTime(new Date());
			baseMachineRemarkChangeService.save(rc);
		}
	}
}
