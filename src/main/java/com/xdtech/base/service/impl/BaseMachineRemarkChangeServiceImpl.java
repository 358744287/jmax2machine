package com.xdtech.base.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xdtech.base.dao.BaseMachineRemarkChangeDao;
import com.xdtech.base.model.BaseMachineRemarkChange;
import com.xdtech.base.service.BaseMachineRemarkChangeService;
import com.xdtech.base.vo.BaseMachineRemarkChangeItem;
import com.xdtech.core.dao.BaseDao;
import com.xdtech.core.model.BaseItem;
import com.xdtech.core.orm.Page;
import com.xdtech.core.orm.utils.BeanUtils;
import com.xdtech.web.model.Pagination;

/**
 * 
 * @author max.zheng
 * @create 2017-03-05 13:19:30
 * @since 1.0
 * @see
 */
@Service
public class BaseMachineRemarkChangeServiceImpl implements BaseMachineRemarkChangeService {
	private Log log = LogFactory.getLog(BaseMachineRemarkChangeServiceImpl.class);
	@Autowired
	private BaseMachineRemarkChangeDao baseMachineRemarkChangeDao;
	@Autowired
	private BaseDao baseDao;
	/**
	 * @description
	 * @author max.zheng
	 * @create 2017-03-05 13:19:30
	 * @modified by
	 * @param entity
	 */
	public void save(BaseMachineRemarkChange entity) {
		baseMachineRemarkChangeDao.save(entity);
	}
	/**
	 * @description
	 * @author max.zheng
	 * @create 2017-03-05 13:19:30
	 * @modified by
	 * @param entity
	 */
	public void saveAll(List<BaseMachineRemarkChange> entities) {
		baseMachineRemarkChangeDao.saveAll(entities);
	}

	/**
	 * @description
	 * @author max.zheng
	 * @create 2017-03-05 13:19:30
	 * @modified by
	 * @param entity
	 */
	public void delete(BaseMachineRemarkChange entity) {
		baseMachineRemarkChangeDao.delete(entity);
	}

	/**
	 * @description
	 * @author max.zheng
	 * @create 2017-03-05 13:19:30
	 * @modified by
	 * @param id
	 */
	public void delete(Long id) {
		baseMachineRemarkChangeDao.delete(id);
	}

	/**
	 * @description
	 * @author max.zheng
	 * @create 2017-03-05 13:19:30
	 * @modified by
	 * @param id
	 * @return
	 */
	public BaseMachineRemarkChange get(Long id) {
		return baseMachineRemarkChangeDao.get(id);
	}

	/**
	 * @description
	 * @author max.zheng
	 * @create 2017-03-05 13:19:30
	 * @modified by
	 * @return
	 */
	public List<BaseMachineRemarkChange> getAll() {
		return baseMachineRemarkChangeDao.getAll();
	}

	/**
	 * @description
	 * @author max.zheng
	 * @create 2017-03-05 13:19:30
	 * @modified by
	 * @param pg
	 * @param values
	 * @return
	 */
	public Map<String, Object> loadPageAndCondition(Pagination pg,
			Map<String, String> values) {
		Map<String, Object> results = new HashMap<String, Object>();
		List<Object> baseMachineRemarkChanges = null;
		long rows = 0;
		if (pg!=null) {
			Page<BaseMachineRemarkChange> page = baseMachineRemarkChangeDao.findPage(pg,"from BaseMachineRemarkChange order by createTime desc", values);
			baseMachineRemarkChanges = BeanUtils.copyListProperties(
					BaseMachineRemarkChangeItem.class, page.getResult());
			rows = page.getTotalItems();
		}else {
			List<BaseMachineRemarkChange> baseMachineRemarkChangeList = baseMachineRemarkChangeDao.find("from BaseMachineRemarkChange order by id desc", values);
			baseMachineRemarkChanges = BeanUtils.copyListProperties(
					BaseMachineRemarkChangeItem.class, baseMachineRemarkChangeList);
			rows = baseMachineRemarkChanges.size();
		}
		results.put("total", rows);
		results.put("rows", baseMachineRemarkChanges);
		return results;
	}

	/**
	 * @description
	 * @author max.zheng
	 * @create 2017-03-05 13:19:30
	 * @modified by
	 * @param item
	 * @return
	 */
	public boolean saveOrUpdateBaseMachineRemarkChange(BaseMachineRemarkChangeItem item) {
		BaseMachineRemarkChange baseMachineRemarkChange = null;
		if (item.getId()==null) {
			baseMachineRemarkChange = new BaseMachineRemarkChange();
		}else {
			baseMachineRemarkChange = baseMachineRemarkChangeDao.get(item.getId());
		}
		//复制前台修改的属性
		BeanUtils.copyProperties(baseMachineRemarkChange, item);
		baseMachineRemarkChangeDao.save(baseMachineRemarkChange);
		return true;
	}

	/**
	 * @description
	 * @author max.zheng
	 * @create 2017-03-05 13:19:30
	 * @modified by
	 * @param baseMachineRemarkChangeId
	 * @return
	 */
	public BaseMachineRemarkChangeItem loadBaseMachineRemarkChangeItem(Long baseMachineRemarkChangeId) {
		BaseMachineRemarkChange baseMachineRemarkChange = baseMachineRemarkChangeDao.get(baseMachineRemarkChangeId);
		return BaseMachineRemarkChangeItem.createItem(baseMachineRemarkChange);
	}

	/**
	 * @description
	 * @author max.zheng
	 * @create 2017-03-05 13:19:30
	 * @modified by
	 * @param id
	 * @return
	 */
	public boolean deleteBaseMachineRemarkChangeInfo(long id) {
		delete(id);
		return true;
	}
	
	/**
	 * @description
	 * @author max.zheng
	 * @create 2017-03-05 13:19:30
	 * @modified by
	 * @param newIds
	 * @return
	 */
	public boolean deleteBaseMachineRemarkChangeInfo(List<Long> baseMachineRemarkChangeIds) {
		for (Long id : baseMachineRemarkChangeIds) {
			delete(id);
		}
		return true;
	}
	
	public Map<String, Object> loadPageDataByConditions(Pagination pg,
			BaseItem baseItem, String queryName) {
		Map<String, Object> results = new HashMap<String, Object>();
		Page page = baseDao.findPageByNamedQuery(pg, queryName,baseItem);
		results.put("total", page.getTotalItems());
		results.put("rows", page.getResult());
		return results;
	}
	/**
	 * @description
	 * @author max.zheng
	 * @create 2017-03-05 13:19:30
	 * @modified by
	 * @return
	 */
	@Override
	public List<BaseMachineRemarkChangeItem> loadItems() {
		List<BaseMachineRemarkChange> baseMachineRemarkChanges = getAll();
		return BaseMachineRemarkChangeItem.createItems(baseMachineRemarkChanges);
	}
	
	public List<BaseMachineRemarkChangeItem> getBaseMachineRemarkChangeByCondition(
			BaseMachineRemarkChangeItem condition) {
		List<BaseMachineRemarkChangeItem> items = baseDao.findByNamedQuery("findBaseMachineRemarkChangeByCondition",condition,condition.loadQueryFields());
		return items;
	}

}
