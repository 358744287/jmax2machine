package com.xdtech.base.service.impl;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xdtech.base.dao.BaseMachineStockDao;
import com.xdtech.base.model.BaseMachineBorrow;
import com.xdtech.base.model.BaseMachineRequestForm;
import com.xdtech.base.model.BaseMachineStock;
import com.xdtech.base.model.BaseMachineType;
import com.xdtech.base.service.BaseMachineBorrowService;
import com.xdtech.base.service.BaseMachineStockService;
import com.xdtech.base.service.BaseMachineTypeService;
import com.xdtech.base.util.ProConstUtil;
import com.xdtech.base.vo.BaseMachineStockItem;
import com.xdtech.common.utils.DateUtil;
import com.xdtech.core.dao.BaseDao;
import com.xdtech.core.model.BaseItem;
import com.xdtech.core.orm.Page;
import com.xdtech.core.orm.utils.BeanUtils;
import com.xdtech.web.model.Pagination;

/**
 * 
 * @author max.zheng
 * @create 2017-03-05 12:18:45
 * @since 1.0
 * @see
 */
@Service
public class BaseMachineStockServiceImpl implements BaseMachineStockService {
	private Log log = LogFactory.getLog(BaseMachineStockServiceImpl.class);
	@Autowired
	private BaseMachineStockDao baseMachineStockDao;
	@Autowired
	private BaseDao baseDao;
	@Autowired
	private BaseMachineTypeService baseMachineTypeService;
	@Autowired
	private BaseMachineBorrowService baseMachineBorrowService;
	/**
	 * @description
	 * @author max.zheng
	 * @create 2017-03-05 12:18:45
	 * @modified by
	 * @param entity
	 */
	public void save(BaseMachineStock entity) {
		baseMachineStockDao.save(entity);
	}
	/**
	 * @description
	 * @author max.zheng
	 * @create 2017-03-05 12:18:45
	 * @modified by
	 * @param entity
	 */
	public void saveAll(List<BaseMachineStock> entities) {
		baseMachineStockDao.saveAll(entities);
	}

	/**
	 * @description
	 * @author max.zheng
	 * @create 2017-03-05 12:18:45
	 * @modified by
	 * @param entity
	 */
	public void delete(BaseMachineStock entity) {
		baseMachineStockDao.delete(entity);
	}

	/**
	 * @description
	 * @author max.zheng
	 * @create 2017-03-05 12:18:45
	 * @modified by
	 * @param id
	 */
	public void delete(Long id) {
		baseMachineStockDao.delete(id);
	}

	/**
	 * @description
	 * @author max.zheng
	 * @create 2017-03-05 12:18:45
	 * @modified by
	 * @param id
	 * @return
	 */
	public BaseMachineStock get(Long id) {
		return baseMachineStockDao.get(id);
	}

	/**
	 * @description
	 * @author max.zheng
	 * @create 2017-03-05 12:18:45
	 * @modified by
	 * @return
	 */
	public List<BaseMachineStock> getAll() {
		return baseMachineStockDao.getAll();
	}

	/**
	 * @description
	 * @author max.zheng
	 * @create 2017-03-05 12:18:45
	 * @modified by
	 * @param pg
	 * @param values
	 * @return
	 */
	public Map<String, Object> loadPageAndCondition(Pagination pg,
			Map<String, String> values) {
		Map<String, Object> results = new HashMap<String, Object>();
		List<Object> baseMachineStocks = null;
		long rows = 0;
		if (pg!=null) {
			Page<BaseMachineStock> page = baseMachineStockDao.findPage(pg,"from BaseMachineStock order by createTime desc", values);
			baseMachineStocks = BeanUtils.copyListProperties(
					BaseMachineStockItem.class, page.getResult());
			rows = page.getTotalItems();
		}else {
			List<BaseMachineStock> baseMachineStockList = baseMachineStockDao.find("from BaseMachineStock order by id desc", values);
			baseMachineStocks = BeanUtils.copyListProperties(
					BaseMachineStockItem.class, baseMachineStockList);
			rows = baseMachineStocks.size();
		}
		results.put("total", rows);
		results.put("rows", baseMachineStocks);
		return results;
	}

	/**
	 * @description
	 * @author max.zheng
	 * @create 2017-03-05 12:18:45
	 * @modified by
	 * @param item
	 * @return
	 */
	public boolean saveOrUpdateBaseMachineStock(BaseMachineStockItem item) {
		BaseMachineStock baseMachineStock = null;
		BaseMachineType baseMachineType = baseMachineTypeService.get(item.getDevTypeId());
		if (item.getId()==null) {
			item.setState(ProConstUtil.STATE_USABLE);
			baseMachineStock = new BaseMachineStock();
		}else {
			baseMachineStock = baseMachineStockDao.get(item.getId());
		}
		//复制前台修改的属性
		BeanUtils.copyProperties(baseMachineStock, item);
		baseMachineStock.setBaseMachineType(baseMachineType);
		baseMachineStockDao.save(baseMachineStock);
		return true;
	}

	/**
	 * @description
	 * @author max.zheng
	 * @create 2017-03-05 12:18:45
	 * @modified by
	 * @param baseMachineStockId
	 * @return
	 */
	public BaseMachineStockItem loadBaseMachineStockItem(Long baseMachineStockId) {
		BaseMachineStock baseMachineStock = baseMachineStockDao.get(baseMachineStockId);
		BaseMachineStockItem baseMachineStockItem = BaseMachineStockItem.createItem(baseMachineStock);;
		baseMachineStockItem.setDevTypeId(baseMachineStock.getBaseMachineType().getId());
		return baseMachineStockItem;
	}

	/**
	 * @description
	 * @author max.zheng
	 * @create 2017-03-05 12:18:45
	 * @modified by
	 * @param id
	 * @return
	 */
	public boolean deleteBaseMachineStockInfo(long id) {
		delete(id);
		return true;
	}
	
	/**
	 * @description
	 * @author max.zheng
	 * @create 2017-03-05 12:18:45
	 * @modified by
	 * @param newIds
	 * @return
	 */
	public boolean deleteBaseMachineStockInfo(List<Long> baseMachineStockIds) {
		for (Long id : baseMachineStockIds) {
			delete(id);
		}
		return true;
	}
	
	public Map<String, Object> loadPageDataByConditions(Pagination pg,
			BaseItem baseItem, String queryName) {
		Map<String, Object> results = new HashMap<String, Object>();
		Page page = baseDao.findPageByNamedQuery(pg, queryName,baseItem);
		results.put("total", page.getTotalItems());
		results.put("rows", page.getResult());
		return results;
	}
	/**
	 * @description
	 * @author max.zheng
	 * @create 2017-03-05 12:18:45
	 * @modified by
	 * @return
	 */
	@Override
	public List<BaseMachineStockItem> loadItems() {
		List<BaseMachineStock> baseMachineStocks = getAll();
		return BaseMachineStockItem.createItems(baseMachineStocks);
	}
	
	public List<BaseMachineStockItem> getBaseMachineStockByCondition(
			BaseMachineStockItem condition) {
		List<BaseMachineStockItem> items = baseDao.findByNamedQuery("findBaseMachineStockByCondition",condition,condition.loadQueryFields());
		return items;
	}

	public boolean updataMachineStockState(String devIds,Integer state)
	{
		if(StringUtils.isNotEmpty(devIds))
		{
			List<BaseMachineStock> devList = baseMachineStockDao.findByHql("from BaseMachineStock where id in (" + devIds + ")");
			for (BaseMachineStock baseMachineStock : devList)
			{
				baseMachineStock.setState(state);
				baseMachineStockDao.save(baseMachineStock);
			}
		}
		return true;
	}
	
	public boolean updataMachineStockState(BaseMachineRequestForm baseMachineRequestForm,Integer state)
	{
		if(StringUtils.isNotEmpty(baseMachineRequestForm.getDevIds()))
		{
			List<BaseMachineStock> devList = baseMachineStockDao.findByHql("from BaseMachineStock where state in (1) and id in (" + baseMachineRequestForm.getDevIds() + ")");
			for (BaseMachineStock dev : devList)
			{
				BaseMachineBorrow borrow = new BaseMachineBorrow();
        		borrow.setBorrowDate(DateUtil.dateToString(new Date(),"yyyy-MM-dd hh:mm:ss"));
        		borrow.setDevId(dev.getId());
        		borrow.setDevSn(dev.getDevSn());
        		borrow.setDevName(dev.getBaseMachineType().getName());
        		borrow.setDevType(dev.getBaseMachineType().getType());
        		borrow.setOrderNo(dev.getOrderNo());
        		borrow.setIntoDate(DateUtil.dateToString(dev.getIntoDate(),"yyyy-MM-dd hh:mm:ss"));
        		borrow.setBorrowDept(baseMachineRequestForm.getDeptName());
        		borrow.setBorrowEmp(baseMachineRequestForm.getName());
        		borrow.setEmpPin(baseMachineRequestForm.getEmpPin());
        		borrow.setRemark(baseMachineRequestForm.getRemark());
        		borrow.setIsReturn(0);
        		baseMachineBorrowService.save(borrow);
        		dev.setState(state);
				baseMachineStockDao.save(dev);
			}
		}
		return true;
	}
	@Override
	public List<BaseMachineStock> findByDevIds(String devIds)
	{
		String hql = "from BaseMachineStock where state not in (1) and ";
		String hqlById = hql + " id in (" + devIds + ")";
		return baseMachineStockDao.findByHql(hqlById);
	}
	
	@Override
	public List<BaseMachineStock> findByOrderNos(String orderNos)
	{
		String hql = "from BaseMachineStock where state not in (1) and ";
		String hqlByNo = hql + " orderNo in (" + orderNos + ")";
		return baseMachineStockDao.findByHql(hqlByNo);
	}
}
