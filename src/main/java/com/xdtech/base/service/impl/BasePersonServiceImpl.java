package com.xdtech.base.service.impl;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.xdtech.core.dao.BaseDao;
import com.xdtech.core.model.BaseItem;
import com.xdtech.core.orm.Page;
import com.xdtech.core.orm.utils.BeanUtils;
import com.xdtech.base.dao.BasePersonDao;
import com.xdtech.base.model.BasePerson;
import com.xdtech.base.service.BasePersonService;
import com.xdtech.base.vo.BasePersonItem;
import com.xdtech.sys.model.User;
import com.xdtech.sys.model.UserGroup;
import com.xdtech.sys.service.UserGroupService;
import com.xdtech.sys.service.UserService;
import com.xdtech.web.model.Pagination;

/**
 * 
 * @author max.zheng
 * @create 2017-03-05 11:52:17
 * @since 1.0
 * @see
 */
@Service
public class BasePersonServiceImpl implements BasePersonService {
	private Log log = LogFactory.getLog(BasePersonServiceImpl.class);
	@Autowired
	private BasePersonDao basePersonDao;
	@Autowired
	private BaseDao baseDao;
	@Autowired
	private UserGroupService userGroupService;
	@Autowired
	private UserService userService;
	/**
	 * @description
	 * @author max.zheng
	 * @create 2017-03-05 11:52:17
	 * @modified by
	 * @param entity
	 */
	public void save(BasePerson entity) {
		basePersonDao.save(entity);
	}
	/**
	 * @description
	 * @author max.zheng
	 * @create 2017-03-05 11:52:17
	 * @modified by
	 * @param entity
	 */
	public void saveAll(List<BasePerson> entities) {
		basePersonDao.saveAll(entities);
	}

	/**
	 * @description
	 * @author max.zheng
	 * @create 2017-03-05 11:52:17
	 * @modified by
	 * @param entity
	 */
	public void delete(BasePerson entity) {
		basePersonDao.delete(entity);
	}

	/**
	 * @description
	 * @author max.zheng
	 * @create 2017-03-05 11:52:17
	 * @modified by
	 * @param id
	 */
	public void delete(Long id) {
		basePersonDao.delete(id);
	}

	/**
	 * @description
	 * @author max.zheng
	 * @create 2017-03-05 11:52:17
	 * @modified by
	 * @param id
	 * @return
	 */
	public BasePerson get(Long id) {
		return basePersonDao.get(id);
	}

	/**
	 * @description
	 * @author max.zheng
	 * @create 2017-03-05 11:52:17
	 * @modified by
	 * @return
	 */
	public List<BasePerson> getAll() {
		return basePersonDao.getAll();
	}

	/**
	 * @description
	 * @author max.zheng
	 * @create 2017-03-05 11:52:17
	 * @modified by
	 * @param pg
	 * @param values
	 * @return
	 */
	public Map<String, Object> loadPageAndCondition(Pagination pg,
			Map<String, String> values) {
		Map<String, Object> results = new HashMap<String, Object>();
		List<Object> basePersons = null;
		long rows = 0;
		if (pg!=null) {
			Page<BasePerson> page = basePersonDao.findPage(pg,"from BasePerson order by createTime desc", values);
//			for (BasePerson basePerson :  page.getResult())
//			{
//				BasePersonItem basePersonItem = BasePersonItem.createItem(basePerson);
//				basePersonItem.setUsergroupName(basePerson.getUserGroup().getName());
//				basePersons.add(basePersonItem);
//			}
			basePersons = BeanUtils.copyListProperties(
					BasePersonItem.class, page.getResult());
			rows = page.getTotalItems();
		}else {
			List<BasePerson> basePersonList = basePersonDao.find("from BasePerson order by id desc", values);
//			for (BasePerson basePerson :  basePersonList)
//			{
//				BasePersonItem basePersonItem = BasePersonItem.createItem(basePerson);
//				basePersonItem.setUsergroupName(basePerson.getUserGroup().getName());
//				basePersons.add(basePersonItem);
//			}
			basePersons = BeanUtils.copyListProperties(
					BasePersonItem.class, basePersonList);
			rows = basePersons.size();
		}
		results.put("total", rows);
		results.put("rows", basePersons);
		return results;
	}

	/**
	 * @description
	 * @author max.zheng
	 * @create 2017-03-05 11:52:17
	 * @modified by
	 * @param item
	 * @return
	 */
	public boolean saveOrUpdateBasePerson(BasePersonItem item) {
		BasePerson basePerson = null;
		UserGroup userGroup = userGroupService.get(item.getUsergroupId());
		if (item.getId()==null) {
			basePerson = new BasePerson();
		}else {
			basePerson = basePersonDao.get(item.getId());
		}
		//复制前台修改的属性
		BeanUtils.copyProperties(basePerson, item);
		basePerson.setUserGroup(userGroup);
		basePersonDao.save(basePerson);
		return true;
	}

	/**
	 * @description
	 * @author max.zheng
	 * @create 2017-03-05 11:52:17
	 * @modified by
	 * @param basePersonId
	 * @return
	 */
	public BasePersonItem loadBasePersonItem(Long basePersonId) {
		BasePerson basePerson = basePersonDao.get(basePersonId);
		BasePersonItem basePersonItem = BasePersonItem.createItem(basePerson);
		basePersonItem.setUsergroupId(basePerson.getUserGroup().getId());
		return basePersonItem;
	}

	/**
	 * @description
	 * @author max.zheng
	 * @create 2017-03-05 11:52:17
	 * @modified by
	 * @param id
	 * @return
	 */
	public boolean deleteBasePersonInfo(long id) {
		delete(id);
		return true;
	}
	
	/**
	 * @description
	 * @author max.zheng
	 * @create 2017-03-05 11:52:17
	 * @modified by
	 * @param newIds
	 * @return
	 */
	public boolean deleteBasePersonInfo(List<Long> basePersonIds) {
		for (Long id : basePersonIds) {
			delete(id);
		}
		return true;
	}
	
	public Map<String, Object> loadPageDataByConditions(Pagination pg,
			BaseItem baseItem, String queryName) {
		Map<String, Object> results = new HashMap<String, Object>();
		Page page = baseDao.findPageByNamedQuery(pg, queryName,baseItem);
		results.put("total", page.getTotalItems());
		results.put("rows", page.getResult());
		return results;
	}
	/**
	 * @description
	 * @author max.zheng
	 * @create 2017-03-05 11:52:17
	 * @modified by
	 * @return
	 */
	@Override
	public List<BasePersonItem> loadItems() {
		List<BasePerson> basePersons = getAll();
		return BasePersonItem.createItems(basePersons);
	}
	
	public List<BasePersonItem> getBasePersonByCondition(
			BasePersonItem condition) {
		List<BasePersonItem> items = baseDao.findByNamedQuery("findBasePersonByCondition",condition,condition.loadQueryFields());
		return items;
	}
	/**
	 * 人员关联系统用户通过userId
	 */
	public boolean createSysUser(BasePersonItem linkSysUser)
	{
		
		return true;
	}
	@Override
	public boolean createUsers(String[] idList)
	{
		BasePerson basePerson = null;
		for(String id:idList)
		{
			basePerson = get(Long.valueOf(id));
			long userId = userService.createUserLinkRole(basePerson.getPin(),basePerson.getName(),"afxtsy");
			basePerson.setUserId(userId);
			save(basePerson);
		}
		return true;
	}
}
