package com.xdtech.base.service;

import java.util.List;

import com.xdtech.common.service.IBaseService;
import com.xdtech.base.model.BaseMachineType;
import com.xdtech.base.vo.BaseMachineTypeItem;

/**
 * 
 * @author max.zheng
 * @create 2017-03-05 12:05:53
 * @since 1.0
 * @see
 */
public interface BaseMachineTypeService extends IBaseService<BaseMachineType>{

	/**
	 * 保存更新信息
	 * @author max.zheng
	 * @create 2017-03-05 12:05:53
	 * @modified by
	 * @param item
	 * @return
	 */
	boolean saveOrUpdateBaseMachineType(BaseMachineTypeItem item);

	/**
	 * 加载记录信息
	 * @author max.zheng
	 * @create 2017-03-05 12:05:53
	 * @modified by
	 * @param newId
	 * @return
	 */
	BaseMachineTypeItem loadBaseMachineTypeItem(Long baseMachineTypeId);

	/**
	 * 根据id号删除记录信息
	 * @author max.zheng
	 * @create 2017-03-05 12:05:53
	 * @modified by
	 * @param id
	 * @return
	 */
	boolean deleteBaseMachineTypeInfo(long id);

	/**
	 * 
	 * @author max.zheng
	 * @create 2017-03-05 12:05:53
	 * @modified by
	 * @param baseMachineTypeIds
	 */
	boolean deleteBaseMachineTypeInfo(List<Long> baseMachineTypeIds);
	
	/**
	 * 加载vo列表条目
	 * @author max.zheng
	 * @create 2017-03-05 12:05:53
	 * @modified by
	 * @return
	 */
	List<BaseMachineTypeItem> loadItems();
	/**
	 * 根据查询器直接查询符合的数据集合，不分页
	 * @author max.zheng
	 * @create 2017-03-05 12:05:53
	 * @modified by
	 * @return
	 */
	public List<BaseMachineTypeItem> getBaseMachineTypeByCondition(
			BaseMachineTypeItem condition);
}
