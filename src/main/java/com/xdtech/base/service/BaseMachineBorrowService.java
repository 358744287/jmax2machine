package com.xdtech.base.service;

import java.util.List;

import com.xdtech.common.service.IBaseService;
import com.xdtech.base.model.BaseMachineBorrow;
import com.xdtech.base.vo.BaseMachineBorrowItem;

/**
 * 
 * @author max.zheng
 * @create 2017-03-05 12:56:55
 * @since 1.0
 * @see
 */
public interface BaseMachineBorrowService extends IBaseService<BaseMachineBorrow>{

	/**
	 * 保存更新信息
	 * @author max.zheng
	 * @create 2017-03-05 12:56:55
	 * @modified by
	 * @param item
	 * @return
	 */
	boolean saveOrUpdateBaseMachineBorrow(BaseMachineBorrowItem item);

	/**
	 * 加载记录信息
	 * @author max.zheng
	 * @create 2017-03-05 12:56:55
	 * @modified by
	 * @param newId
	 * @return
	 */
	BaseMachineBorrowItem loadBaseMachineBorrowItem(Long baseMachineBorrowId);

	/**
	 * 根据id号删除记录信息
	 * @author max.zheng
	 * @create 2017-03-05 12:56:55
	 * @modified by
	 * @param id
	 * @return
	 */
	boolean deleteBaseMachineBorrowInfo(long id);

	/**
	 * 
	 * @author max.zheng
	 * @create 2017-03-05 12:56:55
	 * @modified by
	 * @param baseMachineBorrowIds
	 */
	boolean deleteBaseMachineBorrowInfo(List<Long> baseMachineBorrowIds);
	
	/**
	 * 加载vo列表条目
	 * @author max.zheng
	 * @create 2017-03-05 12:56:55
	 * @modified by
	 * @return
	 */
	List<BaseMachineBorrowItem> loadItems();
	/**
	 * 根据查询器直接查询符合的数据集合，不分页
	 * @author max.zheng
	 * @create 2017-03-05 12:56:55
	 * @modified by
	 * @return
	 */
	public List<BaseMachineBorrowItem> getBaseMachineBorrowByCondition(
			BaseMachineBorrowItem condition);

	void returnBaseMachineBorrow(String ids);

	void updataMachineBorrowRemark(String string, String remark);
}
