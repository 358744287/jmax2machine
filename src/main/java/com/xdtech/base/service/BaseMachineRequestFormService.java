package com.xdtech.base.service;

import java.util.List;

import com.xdtech.common.service.IBaseService;
import com.xdtech.base.model.BaseMachineRequestForm;
import com.xdtech.base.vo.BaseMachineRequestFormItem;

/**
 * 
 * @author max.zheng
 * @create 2017-03-05 12:36:02
 * @since 1.0
 * @see
 */
public interface BaseMachineRequestFormService extends IBaseService<BaseMachineRequestForm>{

	/**
	 * 保存更新信息
	 * @author max.zheng
	 * @create 2017-03-05 12:36:02
	 * @modified by
	 * @param item
	 * @return
	 */
	boolean saveOrUpdateBaseMachineRequestForm(BaseMachineRequestFormItem item);

	/**
	 * 加载记录信息
	 * @author max.zheng
	 * @create 2017-03-05 12:36:02
	 * @modified by
	 * @param newId
	 * @return
	 */
	BaseMachineRequestFormItem loadBaseMachineRequestFormItem(Long baseMachineRequestFormId);

	/**
	 * 根据id号删除记录信息
	 * @author max.zheng
	 * @create 2017-03-05 12:36:02
	 * @modified by
	 * @param id
	 * @return
	 */
	boolean deleteBaseMachineRequestFormInfo(long id);

	/**
	 * 
	 * @author max.zheng
	 * @create 2017-03-05 12:36:02
	 * @modified by
	 * @param baseMachineRequestFormIds
	 */
	boolean deleteBaseMachineRequestFormInfo(List<Long> baseMachineRequestFormIds);
	
	/**
	 * 加载vo列表条目
	 * @author max.zheng
	 * @create 2017-03-05 12:36:02
	 * @modified by
	 * @return
	 */
	List<BaseMachineRequestFormItem> loadItems();
	/**
	 * 根据查询器直接查询符合的数据集合，不分页
	 * @author max.zheng
	 * @create 2017-03-05 12:36:02
	 * @modified by
	 * @return
	 */
	public List<BaseMachineRequestFormItem> getBaseMachineRequestFormByCondition(
			BaseMachineRequestFormItem condition);
	
	public boolean passMachineRequestForm(String baseMachineRequestFormIds);
}
