package com.xdtech.base.dao;

import org.springframework.stereotype.Repository;

import com.xdtech.core.orm.hibernate.HibernateDao;
import com.xdtech.base.model.BaseMachineBorrow;

/**
 * 
 * @author max.zheng
 * @create 2017-03-05 12:56:55
 * @since 1.0
 * @see
 */
@Repository
public class BaseMachineBorrowDao extends HibernateDao<BaseMachineBorrow, Long>{

}
