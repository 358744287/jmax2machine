package com.xdtech.base.dao;

import org.springframework.stereotype.Repository;

import com.xdtech.core.orm.hibernate.HibernateDao;
import com.xdtech.base.model.BasePerson;

/**
 * 
 * @author max.zheng
 * @create 2017-03-05 11:52:17
 * @since 1.0
 * @see
 */
@Repository
public class BasePersonDao extends HibernateDao<BasePerson, Long>{

}
