package com.xdtech.base.dao;

import org.springframework.stereotype.Repository;

import com.xdtech.core.orm.hibernate.HibernateDao;
import com.xdtech.base.model.BaseMachineRequestForm;

/**
 * 
 * @author max.zheng
 * @create 2017-03-05 12:36:02
 * @since 1.0
 * @see
 */
@Repository
public class BaseMachineRequestFormDao extends HibernateDao<BaseMachineRequestForm, Long>
{
	
}
