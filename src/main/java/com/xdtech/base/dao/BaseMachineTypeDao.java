package com.xdtech.base.dao;

import org.springframework.stereotype.Repository;

import com.xdtech.core.orm.hibernate.HibernateDao;
import com.xdtech.base.model.BaseMachineType;

/**
 * 
 * @author max.zheng
 * @create 2017-03-05 12:05:53
 * @since 1.0
 * @see
 */
@Repository
public class BaseMachineTypeDao extends HibernateDao<BaseMachineType, Long>{

}
