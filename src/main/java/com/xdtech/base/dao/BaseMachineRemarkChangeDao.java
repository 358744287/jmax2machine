package com.xdtech.base.dao;

import org.springframework.stereotype.Repository;

import com.xdtech.core.orm.hibernate.HibernateDao;
import com.xdtech.base.model.BaseMachineRemarkChange;

/**
 * 
 * @author max.zheng
 * @create 2017-03-05 13:19:30
 * @since 1.0
 * @see
 */
@Repository
public class BaseMachineRemarkChangeDao extends HibernateDao<BaseMachineRemarkChange, Long>{

}
