package com.xdtech.base.vo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.sql.Timestamp;

import com.xdtech.core.model.BaseItem;
import com.xdtech.core.orm.utils.BeanUtils;
import com.xdtech.base.model.BaseMachineRemarkChange;

import com.xdtech.web.freemark.item.GridColumn;

/**
 * 
 * @author max.zheng
 * @create 2017-03-05 13:19:30
 * @since 1.0
 * @see
 */
public class BaseMachineRemarkChangeItem extends BaseItem implements Serializable{
	private static final long serialVersionUID = 1L;
	private Long id;
	@GridColumn(title="设备SN码",width=150)
	private String devSn;
	@GridColumn(title="设备名称",width=150)
	private String devName;
	@GridColumn(title="固资编码",width=150)
	private String orderNo;
	@GridColumn(title="备注",width=100)
	private String remark;
	@GridColumn(title="变更备注",width=100)
	private String remarkChange;
	@GridColumn(title="变更时间",width=200)
	private String changeTime;
	
	
	public void setId(Long id) {
		this.id = id;
		addQuerys("id", id);
	}
	public Long getId() {
		return id;
	}
	
	public void setDevSn(String devSn) {
		this.devSn = devSn;
		addQuerys("devSn", devSn);
	}
	public String getDevSn() {
		return devSn;
	}
	
	public void setOrderNo(String orderNo) {
		this.orderNo = orderNo;
		addQuerys("orderNo", orderNo);
	}
	public String getOrderNo() {
		return orderNo;
	}
	
	public void setRemark(String remark) {
		this.remark = remark;
		addQuerys("remark", remark);
	}
	public String getRemark() {
		return remark;
	}
	
	public void setRemarkChange(String remarkChange) {
		this.remarkChange = remarkChange;
		addQuerys("remarkChange", remarkChange);
	}
	public String getRemarkChange() {
		return remarkChange;
	}
	
	public void setChangeTime(String changeTime) {
		this.changeTime = changeTime;
		addQuerys("changeTime", changeTime);
	}
	public String getChangeTime() {
		return changeTime;
	}
	
	public void setDevName(String devName) {
		this.devName = devName;
		addQuerys("devName", devName);
	}
	public String getDevName() {
		return devName;
	}
	
	/**
	 * 根据model构建vo
	 * 
	 * @author max
	 * @return
	 */
	public static BaseMachineRemarkChangeItem createItem(BaseMachineRemarkChange baseMachineRemarkChange) {
		BaseMachineRemarkChangeItem baseMachineRemarkChangeItem = null;
		if(baseMachineRemarkChange!=null) {
			baseMachineRemarkChangeItem = new BaseMachineRemarkChangeItem();
			BeanUtils.copyProperties(baseMachineRemarkChangeItem, baseMachineRemarkChange);
			//自定义属性设置填充
		}
		
		return baseMachineRemarkChangeItem;
	}
	/**
	 * 根据model集合创建vo集合
	 * 
	 * @author max
	 * @return
	 */
	public static List<BaseMachineRemarkChangeItem> createItems(List<BaseMachineRemarkChange> baseMachineRemarkChanges) {
		List<BaseMachineRemarkChangeItem> baseMachineRemarkChangeItems = new ArrayList<BaseMachineRemarkChangeItem>();
		for (BaseMachineRemarkChange baseMachineRemarkChange : baseMachineRemarkChanges) {
			baseMachineRemarkChangeItems.add(createItem(baseMachineRemarkChange));
		}
		return baseMachineRemarkChangeItems;
	}
	
	/**
	 * 转model
	 * @return
	 */
	public BaseMachineRemarkChange toModel() {
		BaseMachineRemarkChange model = new BaseMachineRemarkChange();
		BeanUtils.copyProperties(model, this);
		return model;
	}
}
