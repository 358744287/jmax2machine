package com.xdtech.base.vo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.sql.Timestamp;

import com.xdtech.core.model.BaseItem;
import com.xdtech.core.orm.utils.BeanUtils;
import com.xdtech.base.model.BasePerson;
import com.xdtech.sys.util.SysConstants;
import com.xdtech.web.freemark.item.GridColumn;

/**
 * 
 * @author max.zheng
 * @create 2017-03-05 11:52:17
 * @since 1.0
 * @see
 */
public class BasePersonItem extends BaseItem implements Serializable
{
	private static final long serialVersionUID = 1L;
	private Long id;
	@GridColumn(title = "人员编号", width = 150)
	private String pin;
	@GridColumn(title = "姓名", width = 150)
	private String name;
	//是否创建用户
	@GridColumn(title = "用户ID", width = 150,show=false)
	private Long usergroupId;
	@GridColumn(title = "部门", width = 200)
	private String usergroupName;
	@GridColumn(title = "是否创建系统用户", width = 200,formatter={"N=<span style=\"color:red;\">否</span>","Y=<span style=\"color:green;\">是</span>"})
	private String linkSysUser;

	public void setId(Long id)
	{
		this.id = id;
		addQuerys("id", id);
	}

	public Long getId()
	{
		return id;
	}

	public void setPin(String pin)
	{
		this.pin = pin;
		addQuerys("pin", pin);
	}

	public String getPin()
	{
		return pin;
	}

	public void setName(String name)
	{
		this.name = name;
		addQuerys("name", name);
	}

	public String getName()
	{
		return name;
	}
	
	public Long getUsergroupId()
	{
		return usergroupId;
	}

	public void setUsergroupId(Long usergroupId)
	{
		this.usergroupId = usergroupId;
	}

	public String getUsergroupName()
	{
		return usergroupName;
	}

	public void setUsergroupName(String usergroupName)
	{
		this.usergroupName = usergroupName;
	}
	
	public String getLinkSysUser()
	{
		return linkSysUser;
	}

	public void setLinkSysUser(String linkSysUser)
	{
		this.linkSysUser = linkSysUser;
	}

	/**
	 * 根据model构建vo
	 * 
	 * @author max
	 * @return
	 */
	public static BasePersonItem createItem(BasePerson basePerson)
	{
		BasePersonItem basePersonItem = null;
		if (basePerson != null)
		{
			basePersonItem = new BasePersonItem();
			BeanUtils.copyProperties(basePersonItem, basePerson);
			// 自定义属性设置填充
			basePersonItem.setLinkSysUser(basePerson.getUserId()!=null?SysConstants.YES:SysConstants.NO);
		}

		return basePersonItem;
	}

	/**
	 * 根据model集合创建vo集合
	 * 
	 * @author max
	 * @return
	 */
	public static List<BasePersonItem> createItems(List<BasePerson> basePersons)
	{
		List<BasePersonItem> basePersonItems = new ArrayList<BasePersonItem>();
		for (BasePerson basePerson : basePersons)
		{
			basePersonItems.add(createItem(basePerson));
		}
		return basePersonItems;
	}

	/**
	 * 转model
	 * 
	 * @return
	 */
	public BasePerson toModel()
	{
		BasePerson model = new BasePerson();
		BeanUtils.copyProperties(model, this);
		return model;
	}
}
