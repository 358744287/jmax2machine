package com.xdtech.base.vo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.sql.Timestamp;

import com.xdtech.core.model.BaseItem;
import com.xdtech.core.orm.utils.BeanUtils;
import com.xdtech.base.model.BaseMachineStock;

import com.xdtech.web.freemark.item.GridColumn;

/**
 * 
 * @author max.zheng
 * @create 2017-03-05 12:18:45
 * @since 1.0
 * @see
 */
public class BaseMachineStockItem extends BaseItem implements Serializable
{
	private static final long serialVersionUID = 1L;
	private Long id;
	@GridColumn(title = "设备型号", width = 150)
	private String devTypeType;
	@GridColumn(title = "设备名称", width = 150)
	private String devTypeName;
	
	@GridColumn(title = "设备SN码", width = 150)
	private String devSn;
	@GridColumn(title = "固资编码", width = 150)
	private String orderNo;
	@GridColumn(title = "备注", width = 100)
	private String remark;
	@GridColumn(title = "状态", width = 100,formatter={"1=可用","2=借出","3=丢失","4=维修","5=返厂翻新"})
	private Integer state;
	@GridColumn(title = "入库时间", width = 150)
	private String intoDate;
	@GridColumn(title = "入库数量", width = 100)
	private Integer counts;
	
	private Long devTypeId;

	public void setId(Long id)
	{
		this.id = id;
		addQuerys("id", id);
	}

	public Long getId()
	{
		return id;
	}

	public void setDevSn(String devSn)
	{
		this.devSn = devSn;
		addQuerys("devSn", devSn);
	}

	public String getDevSn()
	{
		return devSn;
	}

	public void setOrderNo(String orderNo)
	{
		this.orderNo = orderNo;
		addQuerys("orderNo", orderNo);
	}

	public String getOrderNo()
	{
		return orderNo;
	}

	public void setRemark(String remark)
	{
		this.remark = remark;
		addQuerys("remark", remark);
	}

	public String getRemark()
	{
		return remark;
	}

	public void setState(Integer state)
	{
		this.state = state;
		addQuerys("state", state);
	}

	public Integer getState()
	{
		return state;
	}

	public void setIntoDate(String intoDate)
	{
		this.intoDate = intoDate;
		addQuerys("intoDate", intoDate);
	}

	public String getIntoDate()
	{
		return intoDate;
	}

	public void setCounts(Integer counts)
	{
		this.counts = counts;
		addQuerys("counts", counts);
	}

	public Integer getCounts()
	{
		return counts;
	}

	public Long getDevTypeId()
	{
		return devTypeId;
	}

	public void setDevTypeId(Long devTypeId)
	{
		this.devTypeId = devTypeId;
	}

	public String getDevTypeType()
	{
		return devTypeType;
	}

	public void setDevTypeType(String devTypeType)
	{
		this.devTypeType = devTypeType;
		addQuerys("devTypeType", devTypeType);
	}

	public String getDevTypeName()
	{
		return devTypeName;
	}

	public void setDevTypeName(String devTypeName)
	{
		this.devTypeName = devTypeName;
	}

	/**
	 * 根据model构建vo
	 * 
	 * @author max
	 * @return
	 */
	public static BaseMachineStockItem createItem(
			BaseMachineStock baseMachineStock)
	{
		BaseMachineStockItem baseMachineStockItem = null;
		if (baseMachineStock != null)
		{
			baseMachineStockItem = new BaseMachineStockItem();
			BeanUtils.copyProperties(baseMachineStockItem, baseMachineStock);
			// 自定义属性设置填充
		}

		return baseMachineStockItem;
	}

	/**
	 * 根据model集合创建vo集合
	 * 
	 * @author max
	 * @return
	 */
	public static List<BaseMachineStockItem> createItems(
			List<BaseMachineStock> baseMachineStocks)
	{
		List<BaseMachineStockItem> baseMachineStockItems = new ArrayList<BaseMachineStockItem>();
		for (BaseMachineStock baseMachineStock : baseMachineStocks)
		{
			baseMachineStockItems.add(createItem(baseMachineStock));
		}
		return baseMachineStockItems;
	}

	/**
	 * 转model
	 * 
	 * @return
	 */
	public BaseMachineStock toModel()
	{
		BaseMachineStock model = new BaseMachineStock();
		BeanUtils.copyProperties(model, this);
		return model;
	}
}
