package com.xdtech.base.vo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.sql.Timestamp;

import com.xdtech.core.model.BaseItem;
import com.xdtech.core.orm.utils.BeanUtils;
import com.xdtech.base.model.BaseMachineRequestForm;

import com.xdtech.web.freemark.item.GridColumn;

/**
 * 
 * @author max.zheng
 * @create 2017-03-05 12:36:02
 * @since 1.0
 * @see
 */
public class BaseMachineRequestFormItem extends BaseItem implements Serializable{
	private static final long serialVersionUID = 1L;
	private Long id;
	@GridColumn(title="部门",width=150)
	private String deptName;
	@GridColumn(title="工号",width=100)
	private String empPin;
	@GridColumn(title="姓名",width=100)
	private String name;
	@GridColumn(title="申请的机器ID",width=100,show=false)
	private String devIds;
	@GridColumn(title="借用机器",width=300)
	private String requestContent;
	@GridColumn(title="申请时间",width=200)
	private String requestDate;
	@GridColumn(title="审批状态",width=100,formatter={"1=未审批","2=审批通过"})
	private Integer requestState;
	@GridColumn(title="审批时间",width=200)
	private String responseDate;
	@GridColumn(title="备注",width=100)
	private String remark;

	public void setId(Long id) {
		this.id = id;
		addQuerys("id", id);
	}
	public Long getId() {
		return id;
	}
	
	public void setDeptName(String deptName) {
		this.deptName = deptName;
		addQuerys("deptName", deptName);
	}
	public String getDeptName() {
		return deptName;
	}
	
	public void setEmpPin(String empPin) {
		this.empPin = empPin;
		addQuerys("empPin", empPin);
	}
	public String getEmpPin() {
		return empPin;
	}
	
	public void setName(String name) {
		this.name = name;
		addQuerys("name", name);
	}
	public String getName() {
		return name;
	}
	
	public void setDevIds(String devIds) {
		this.devIds = devIds;
		addQuerys("devIds", devIds);
	}
	public String getDevIds() {
		return devIds;
	}
	
	public void setRequestContent(String requestContent) {
		this.requestContent = requestContent;
		addQuerys("requestContent", requestContent);
	}
	public String getRequestContent() {
		return requestContent;
	}
	
	public void setRequestDate(String requestDate) {
		this.requestDate = requestDate;
		addQuerys("requestDate", requestDate);
	}
	public String getRequestDate() {
		return requestDate;
	}
	
	public void setRequestState(Integer requestState) {
		this.requestState = requestState;
		addQuerys("requestState", requestState);
	}
	public Integer getRequestState() {
		return requestState;
	}
	
	public void setResponseDate(String responseDate) {
		this.responseDate = responseDate;
		addQuerys("responseDate", responseDate);
	}
	public String getResponseDate() {
		return responseDate;
	}
	
	public void setRemark(String remark) {
		this.remark = remark;
		addQuerys("remark", remark);
	}
	public String getRemark() {
		return remark;
	}
	
	/**
	 * 根据model构建vo
	 * 
	 * @author max
	 * @return
	 */
	public static BaseMachineRequestFormItem createItem(BaseMachineRequestForm baseMachineRequestForm) {
		BaseMachineRequestFormItem baseMachineRequestFormItem = null;
		if(baseMachineRequestForm!=null) {
			baseMachineRequestFormItem = new BaseMachineRequestFormItem();
			BeanUtils.copyProperties(baseMachineRequestFormItem, baseMachineRequestForm);
			//自定义属性设置填充
		}
		
		return baseMachineRequestFormItem;
	}
	/**
	 * 根据model集合创建vo集合
	 * 
	 * @author max
	 * @return
	 */
	public static List<BaseMachineRequestFormItem> createItems(List<BaseMachineRequestForm> baseMachineRequestForms) {
		List<BaseMachineRequestFormItem> baseMachineRequestFormItems = new ArrayList<BaseMachineRequestFormItem>();
		for (BaseMachineRequestForm baseMachineRequestForm : baseMachineRequestForms) {
			baseMachineRequestFormItems.add(createItem(baseMachineRequestForm));
		}
		return baseMachineRequestFormItems;
	}
	
	/**
	 * 转model
	 * @return
	 */
	public BaseMachineRequestForm toModel() {
		BaseMachineRequestForm model = new BaseMachineRequestForm();
		BeanUtils.copyProperties(model, this);
		return model;
	}
}
