package com.xdtech.base.vo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.sql.Timestamp;

import com.xdtech.core.model.BaseItem;
import com.xdtech.core.orm.utils.BeanUtils;
import com.xdtech.base.model.BaseMachineBorrow;

import com.xdtech.web.freemark.item.GridColumn;

/**
 * 
 * @author max.zheng
 * @create 2017-03-05 12:56:55
 * @since 1.0
 * @see
 */
public class BaseMachineBorrowItem extends BaseItem implements Serializable{
	private static final long serialVersionUID = 1L;
	private Long id;
	@GridColumn(title="设备ID",width=100,show=false)
	private Integer devId;
	@GridColumn(title="机器名称",width=200)
	private String devName;
	@GridColumn(title="机器型号",width=150)
	private String devType;
	@GridColumn(title="设备SN码",width=200)
	private String devSn;
	@GridColumn(title="固资编码",width=150)
	private String orderNo;
	@GridColumn(title="部门",width=150)
	private String borrowDept;
	@GridColumn(title="借用人",width=100)
	private String borrowEmp;
	@GridColumn(title="工号",width=100)
	private String empPin;
	@GridColumn(title="借用时间",width=200)
	private String borrowDate;
	@GridColumn(title="归还时间",width=200)
	private String returnDate;
	@GridColumn(title="备注",width=100)
	private String remark;
	@GridColumn(title="入库时间",width=200)
	private String intoDate;
	@GridColumn(title="是否归还",width=100,formatter={"1=已归还","0=未归还"})
	private Integer isReturn;

	public void setId(Long id) {
		this.id = id;
		addQuerys("id", id);
	}
	public Long getId() {
		return id;
	}
	
	public void setDevId(Integer devId) {
		this.devId = devId;
		addQuerys("devId", devId);
	}
	public Integer getDevId() {
		return devId;
	}
	
	public void setDevName(String devName) {
		this.devName = devName;
		addQuerys("devName", devName);
	}
	public String getDevName() {
		return devName;
	}
	
	public void setDevType(String devType) {
		this.devType = devType;
		addQuerys("devType", devType);
	}
	public String getDevType() {
		return devType;
	}
	
	public void setDevSn(String devSn) {
		this.devSn = devSn;
		addQuerys("devSn", devSn);
	}
	public String getDevSn() {
		return devSn;
	}
	
	public void setOrderNo(String orderNo) {
		this.orderNo = orderNo;
		addQuerys("orderNo", orderNo);
	}
	public String getOrderNo() {
		return orderNo;
	}
	
	public void setBorrowDept(String borrowDept) {
		this.borrowDept = borrowDept;
		addQuerys("borrowDept", borrowDept);
	}
	public String getBorrowDept() {
		return borrowDept;
	}
	
	public void setBorrowEmp(String borrowEmp) {
		this.borrowEmp = borrowEmp;
		addQuerys("borrowEmp", borrowEmp);
	}
	public String getBorrowEmp() {
		return borrowEmp;
	}
	
	public void setEmpPin(String empPin) {
		this.empPin = empPin;
		addQuerys("empPin", empPin);
	}
	public String getEmpPin() {
		return empPin;
	}
	
	public void setBorrowDate(String borrowDate) {
		this.borrowDate = borrowDate;
		addQuerys("borrowDate", borrowDate);
	}
	public String getBorrowDate() {
		return borrowDate;
	}
	
	public void setReturnDate(String returnDate) {
		this.returnDate = returnDate;
		addQuerys("returnDate", returnDate);
	}
	public String getReturnDate() {
		return returnDate;
	}
	
	public void setRemark(String remark) {
		this.remark = remark;
		addQuerys("remark", remark);
	}
	public String getRemark() {
		return remark;
	}
	
	public void setIntoDate(String intoDate) {
		this.intoDate = intoDate;
		addQuerys("intoDate", intoDate);
	}
	public String getIntoDate() {
		return intoDate;
	}
	
	public void setIsReturn(Integer isReturn) {
		this.isReturn = isReturn;
		addQuerys("isReturn", isReturn);
	}
	public Integer getIsReturn() {
		return isReturn;
	}
	
	/**
	 * 根据model构建vo
	 * 
	 * @author max
	 * @return
	 */
	public static BaseMachineBorrowItem createItem(BaseMachineBorrow baseMachineBorrow) {
		BaseMachineBorrowItem baseMachineBorrowItem = null;
		if(baseMachineBorrow!=null) {
			baseMachineBorrowItem = new BaseMachineBorrowItem();
			BeanUtils.copyProperties(baseMachineBorrowItem, baseMachineBorrow);
			//自定义属性设置填充
		}
		
		return baseMachineBorrowItem;
	}
	/**
	 * 根据model集合创建vo集合
	 * 
	 * @author max
	 * @return
	 */
	public static List<BaseMachineBorrowItem> createItems(List<BaseMachineBorrow> baseMachineBorrows) {
		List<BaseMachineBorrowItem> baseMachineBorrowItems = new ArrayList<BaseMachineBorrowItem>();
		for (BaseMachineBorrow baseMachineBorrow : baseMachineBorrows) {
			baseMachineBorrowItems.add(createItem(baseMachineBorrow));
		}
		return baseMachineBorrowItems;
	}
	
	/**
	 * 转model
	 * @return
	 */
	public BaseMachineBorrow toModel() {
		BaseMachineBorrow model = new BaseMachineBorrow();
		BeanUtils.copyProperties(model, this);
		return model;
	}
}
