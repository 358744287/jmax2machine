package com.xdtech.base.vo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.sql.Timestamp;

import com.xdtech.core.model.BaseItem;
import com.xdtech.core.orm.utils.BeanUtils;
import com.xdtech.base.model.BaseMachineType;

import com.xdtech.web.freemark.item.GridColumn;

/**
 * 
 * @author max.zheng
 * @create 2017-03-05 12:05:53
 * @since 1.0
 * @see
 */
public class BaseMachineTypeItem extends BaseItem implements Serializable{
	private static final long serialVersionUID = 1L;
	private Long id;
	@GridColumn(title="机器型号",width=200)
	private String type;
	@GridColumn(title="机器名称",width=200)
	private String name;
	@GridColumn(title="品牌",width=200)
	private String brand;

	public void setId(Long id) {
		this.id = id;
		addQuerys("id", id);
	}
	public Long getId() {
		return id;
	}
	
	public void setType(String type) {
		this.type = type;
		addQuerys("type", type);
	}
	public String getType() {
		return type;
	}
	
	public void setName(String name) {
		this.name = name;
		addQuerys("name", name);
	}
	public String getName() {
		return name;
	}
	
	public void setBrand(String brand) {
		this.brand = brand;
		addQuerys("brand", brand);
	}
	public String getBrand() {
		return brand;
	}
	
	/**
	 * 根据model构建vo
	 * 
	 * @author max
	 * @return
	 */
	public static BaseMachineTypeItem createItem(BaseMachineType baseMachineType) {
		BaseMachineTypeItem baseMachineTypeItem = null;
		if(baseMachineType!=null) {
			baseMachineTypeItem = new BaseMachineTypeItem();
			BeanUtils.copyProperties(baseMachineTypeItem, baseMachineType);
			//自定义属性设置填充
		}
		
		return baseMachineTypeItem;
	}
	/**
	 * 根据model集合创建vo集合
	 * 
	 * @author max
	 * @return
	 */
	public static List<BaseMachineTypeItem> createItems(List<BaseMachineType> baseMachineTypes) {
		List<BaseMachineTypeItem> baseMachineTypeItems = new ArrayList<BaseMachineTypeItem>();
		for (BaseMachineType baseMachineType : baseMachineTypes) {
			baseMachineTypeItems.add(createItem(baseMachineType));
		}
		return baseMachineTypeItems;
	}
	
	/**
	 * 转model
	 * @return
	 */
	public BaseMachineType toModel() {
		BaseMachineType model = new BaseMachineType();
		BeanUtils.copyProperties(model, this);
		return model;
	}
}
