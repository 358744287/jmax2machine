package com.xdtech.machine.init;

import java.util.List;

import com.xdtech.common.service.impl.BaseService;
import com.xdtech.common.utils.EmptyUtil;
import com.xdtech.core.model.BaseModel;
import com.xdtech.sys.init.SysAbstractInit;
import com.xdtech.sys.model.MenuFunction;
import com.xdtech.sys.model.Params;

public class MachineInit extends SysAbstractInit
{

    @Override
    public void initingToDb(BaseService<BaseModel> baseService)
    {
        this.baseService = baseService;
        List params = (List) baseService.findByHql("from Params where code=? and codeValue=?", "MachineInit", "true");
        log.info("机器系统模块数据库初始化......");
        if (EmptyUtil.isEmpty(params))
        {
            initData(baseService);
            Params p = new Params("MachineInit", "true", "机器系统模块数据库初始化标记");
            baseService.save(p);
            log.info("机器系统模块数据库初始化......结束");
        }
        else
        {
            log.info("机器系统模块数据库已经初始化......完毕");
        }
    }

    private void initData(BaseService<BaseModel> baseService)
    {
        MenuFunction cmsMf = new MenuFunction();
        // 机器管理系统主菜单
        cmsMf.setIconName("icon-session");
        cmsMf.setNameCN("设备管理");
        cmsMf.setOperationCode("machine-system");
        cmsMf.setPageUrl(null);
        baseService.save(cmsMf);
        // 二级菜单 - 人员管理
        MenuFunction bpmM = new MenuFunction();
        bpmM.setIconName("icon-save");
        bpmM.setNameCN("人员");
        bpmM.setOperationCode("machine-person-manage");
        bpmM.setPageUrl("basePerson.do?basePerson");
        bpmM.setParent(cmsMf);
        baseService.save(bpmM);
        initButtonCodeByPage(bpmM, "base/basePerson/basePerson.html");
        
        // 二级菜单 - 机器类型管理
        MenuFunction bpm2M = new MenuFunction();
        bpm2M.setIconName("icon-save");
        bpm2M.setNameCN("机器类型");
        bpm2M.setOperationCode("machine-type-manage");
        bpm2M.setPageUrl("baseMachineType.do?baseMachineType");
        bpm2M.setParent(cmsMf);
        baseService.save(bpm2M);
        initButtonCodeByPage(bpm2M, "base/baseMachineType/baseMachineType.html");
        // 二级菜单 - 库存机器管理
        MenuFunction bpm3M = new MenuFunction();
        bpm3M.setIconName("icon-save");
        bpm3M.setNameCN("库存机器");
        bpm3M.setOperationCode("machine-stock-manage");
        bpm3M.setPageUrl("baseMachineStock.do?baseMachineStock");
        bpm3M.setParent(cmsMf);
        baseService.save(bpm3M);
        initButtonCodeByPage(bpm3M, "base/baseMachineStock/baseMachineStock.html");
        // 二级菜单 - 机器借用申请单管理
        MenuFunction bpm4M = new MenuFunction();
        bpm4M.setIconName("icon-save");
        bpm4M.setNameCN("机器借用申请单");
        bpm4M.setOperationCode("machine-request-form-manage");
        bpm4M.setPageUrl("baseMachineRequestForm.do?baseMachineRequestForm");
        bpm4M.setParent(cmsMf);
        baseService.save(bpm4M);
        initButtonCodeByPage(bpm4M, "base/baseMachineRequestForm/baseMachineRequestForm.html");
        // 二级菜单 - 机器借用明细管理
        MenuFunction bpm5M = new MenuFunction();
        bpm5M.setIconName("icon-save");
        bpm5M.setNameCN("机器借用明细");
        bpm5M.setOperationCode("machine-borrow-manage");
        bpm5M.setPageUrl("baseMachineBorrow.do?baseMachineBorrow");
        bpm5M.setParent(cmsMf);
        baseService.save(bpm5M);
        initButtonCodeByPage(bpm5M, "base/baseMachineBorrow/baseMachineBorrow.html");
        // 二级菜单 - 借用明细更改管理
        MenuFunction bpm6M = new MenuFunction();
        bpm6M.setIconName("icon-save");
        bpm6M.setNameCN("借用明细更改");
        bpm6M.setOperationCode("machine-remark-change-manage");
        bpm6M.setPageUrl("baseMachineRemarkChange.do?baseMachineRemarkChange");
        bpm6M.setParent(cmsMf);
        baseService.save(bpm6M);
        initButtonCodeByPage(bpm5M, "base/baseMachineRemarkChange/baseMachineRemarkChange.html");
    }

    @Override
    public int getInitOrder()
    {
        return 100;
    }

    @Override
    public void initingToCache(BaseService<BaseModel> baseService)
    {
    }

    @Override
    public void initingToWebApplication(BaseService<BaseModel> baseService)
    {
    }

}
